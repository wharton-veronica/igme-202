/**
 * Veronica Wharton
 * IGME-202-02
 * HW #1: Drawing Application
 */

/**
 * A class that represents a bouncing ball.
 */
class Ball {
  // static class members
  float ACCEL = 1.5; // + is down, - is up
  
  // private object members
  float diameter;    // diameter of ball
  PVector location;  // location of ball's center
  color col;         // color of ball
  float v;           // current velocity of ball
   
  // public
  Ball(float d, PVector loc, color c) {
    diameter = d; 
    location = loc;
    v = 0.0; 
    col = c; ;  
  }
 
  // public
  void render() {
    // move the ball
    move(); 
    
    // draw the ball
    fill(col); 
    noStroke(); 
    ellipse(location.x,location.y,diameter,diameter); 
  } 
  
  // private
  void move() {
    v += ACCEL;
    location.y += v;
  
    // check for collision with bottom of frame
    if (location.y+diameter/2 >= height) {
      v = 0-v; 
      location.y = height-diameter/2;  
    }
  }
}
