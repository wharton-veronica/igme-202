class AutoCircle extends Circle {
  PVector v; 
    
  /** 
   * Constructor for an AutoCircle
   * @param rad The circle's radius
   * @param c The circle's (default) fill color 
   */
  AutoCircle(int rad, color c, PVector v) {
    this(rad, c, null, v);
  }
  
  /** 
   * Constructor for an AutoCircle
   * @param rad The circle's radius
   * @param c The circle's (default) fill color 
   * @param pos The circle's center coordinates
   */
  AutoCircle(int rad, color c, PVector pos, PVector v) {
    super(rad, c, pos); 
    this.v = v;    
  }
  
  void move() {
    
  }
  
}
