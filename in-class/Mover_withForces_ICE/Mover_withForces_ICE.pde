// The Nature of Code
// Daniel Shiffman
// http://natureofcode.coM
//Used for IGME 202

//@author Veronica Wharton (vlw1338)

//IGME 202 Exercise - Wednesday, 3/12/14 - Snow Day -----------------------------------
//DIRECTIONS --------------------------------------------------------------------------
/*
Notes:
I have started this program with very simple code.  There is one Mover declared.  
Follow the directions below to implement this program.  Follow along with Daniel Shiffman's
Nature of Code book, Chapter 2 Forces.  Watch his videos on Chapter 2 for more clarification. 

1. Create multiple Mover objects.  These Movers are affected by multiple forces 
such as wind, gravity, air resistance, and friction.
(Hint - an efficient easy to create multiple Movers is through using an array list)

2. Write an applyForce(PVector forcename) function that correctly applies a force
to a Mover object.

3. Declare some forces as PVectors.  Apply these forces to the Mover objects. 

4. Write a calculateFriction function that correctly calculates the force of friction
based upon the object's mass.  See http://natureofcode.com/book/chapter-2-forces/ 
for more details.

5. Save your code as "YourName_ForcesICE" inside a zipped folder with the same name.  
Upload your code to the MyCourses dropbox called Mover with Forces and Friction.

Follow the code comments below.  Directions are in all-caps. 
*/


//Program-wide variables -------------------------------------------------
//CREATE MULTIPLE MOVER OBJECTS HERE
int NUM_MOVERS = 10; 
Mover[] movers = new Mover[NUM_MOVERS];

//DECLARE 2 NEW FORCES: WIND AND GRAVITY HERE
PVector wind, gravity, upwardWind, sidewaysWind; 

//setup() ------------------------------------------------------------------
void setup() {
  size(640,360);
  
  //INSTANTIATE MULTIPLE MOVER OBJECTS HERE
  //EX = MOVERNAME = NEW MOVER(MASS, RADIUS)
  for (int i = 0; i < movers.length; i++) {
    movers[i] = new Mover(random(0,1), int(random(20,30))); 
  }  
  
  //INSTANTIATE WIND FORCE AND GRAVITY FORCE HERE
  //EX: PVECTOR FORCENAME = NEW PVECTOR(0, 0);
  //SET THE WIND FORCE TO (0.1, 0)
  //SET GRAVITY TO (0, 0.5)
  wind = new PVector(0.1, 0); 
  gravity = new PVector(0, 0.5);
  upwardWind = new PVector(0.2, 0); 
  sidewaysWind = new PVector(-0.1, 0.4); 

}

//draw() -------------------------------------------------------------------
void draw() {
  background(255);
  
  for (Mover m : movers) {
    //CALL APPLYFORCE() METHOD ON EACH MOVER OBJECT
    //APPLY WIND AND GRAVITY TO EACH MOVER OBJECT
    m.applyForce(wind);
    m.applyForce(gravity);
    if (random(0,1) < 0.25) {
      m.applyForce(upwardWind);  
    }
    
    if (random(0,1) < 0.15) {
      m.applyForce(sidewaysWind);  
    }
    
    //CALL CALCULATE FRICTION ON EACH MOVER OBJECT
    m.calculateFriction();
    
    //UPDATE EACH MOVER'S POSITION
    m.update();
    
    //DRAW EACH MOVER TO THE WINDOW
    m.display();
    
    //KEEP EACH MOVER WITHIN THE BOUNDS OF THE WINDOW
    m.checkEdges();
  }
}

