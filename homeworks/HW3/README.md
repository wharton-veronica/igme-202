HW #3: Asteroids
-----------------
**Veronica Wharton (IGME-202-02)**

### Overview

This program is a recreation of [Asteroids](http://www.freeasteroids.org).  The game begins with a ship (user-controlled) in the center of the screen.  Between 10 and 20 randomly-sized asteroids with constant velocities are moving around the screen.  The ship's objective is to use its bullets to hit and destroy the asteroids.  The user's score -- the number of asteroids that the user has successfully destroyed -- is shown in the upper-left corner of the window.  When the ship hits an asteroid, its stroke color will be changed (although the ship cannot truly "die.")  New asteroids are added to the screen when less than a third of the original number of asteroids remain, so the user can  continue to play indefinitely. 

Hot keys and their respective functionalities are as follows: 

Hot Key | Functionality
--------|---------------
RIGHT   | Rotate CW
LEFT    | Rotate CCW
UP      | Accelerate
SPACE   | Fire a bullet
a/A 	| Take a screenshot of the current frame

### Design

The game's aesthetic and functionality are meant to mimic those of the original 1979 game.  Notable deviations from the original game include: circular asteroids and a lack of "death" state for the ship (play can continue indefinitely). 

### Implementation

As part of this game, there are Ship, Asteroid, and Bullet classes.  Each class controls its own movement -- the main class only calls each object's "render" method once per frame.  Acceleration and friction are implemented for the ship's movement.  The movements of the bullets and asteroids are not affected by either acceleration or friction.  The ship also has a terminal velocity.  Also, although Processing cannot enforce true information hiding, getter methods were used created for accessing instance variables (in lieu of the "dot" notation for accessing an object's member variables.)

Additionally, all game components' positions "wrap around" the edge of a screen (e.g., if a ship, asteroid, or bullet flies off the right side of the screen, it will reappear at the same y-coordinate on the left side of the screen).  The 'wraparound' method is a global method (effectively a static method) that can be used by all classes.  

Bounding circle collision detection is implemented between the asteroids and the ship and between the asteroids and the bullets.  Collision detection is implemented globally rather than in objects.  Collisions between asteroids and bullets are resolved by both objects disappearing (i.e., being removed from their respective data structures) and the user's score being incremented by one.  Collisions between asteroids and the ships are resolved by the ship's stroke randomly changing color.  Collisions between bullets and the ship are not tested for -- the ship is effectively "immune" to its own bullets.

New asteroids are generated a two points during the game: 1) at the beginning of the game and 2) when the number of remaining asteroids is less than one third than the original number of asteroids.  In each case, an asteroid's starting position is always at an edge of the window -- there is an equal probability of an asteroid being generated along each of the four window edges.  This way, there is no chance of a user immediately "dying" due to an asteroid being generated at the ship's starting position.  It also allows the user a few seconds to get their bearing.

Finally, the user can take a screenshot of the current frame by pressing the 'a' key.

In conclusion, I had fun making this game!  I am especially fond of the flame that appears when the ship is accelerating.  I hope that you enjoy playing the game!
