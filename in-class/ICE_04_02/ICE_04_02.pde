/** 
 * IGME-202-02
 * @author Veronica Wharton (vlw1338)
 */
 
ArrayList<VehicleShell> vehicles; 
ArrayList<Light> lights; 

void setup() {
  size(800,600,P2D);
 
  vehicles = new ArrayList<VehicleShell>(); 
  lights = new ArrayList<Light>(); 
  
  // create 2 vehicles
  for (int i = 0; i < 2; i++) {
    vehicles.add(new VehicleShell(
      new PVector(random(100,500), random(100,500)), // starting location
      2.5, // mass
      20., 
      30.
    )); 
  }
  
  // create 2 lights
  for (int i = 0; i < 2; i++) {
    lights.add(new Light(
      new PVector(random(500,700), random(100,500)), // starting location
      int(random(10,100))
    )); 
  } 
    
}

void draw() {
  background(50); 
  
  for (VehicleShell v : vehicles) {
    v.display();  
  }
  
  for (Light l : lights) {
    l.display();  
  }
}
