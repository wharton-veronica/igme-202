/** 
 * IGME-202-02
 * @author Veronica Wharton (vlw1338)
 */

class VehicleShell {
  PVector pos; 
  PShape sensorL, sensorR, body; 
  float vWidth, vHeight, sideDistance, aheadDistance; 
  float mass; 
  
  int DIAM = 10;
  float OFFSET = 1;  
  
  /**
   * Constructor for a VehicleShell
   * 
   * @param pos Starting position of vehicle
   * @param mass Mass of vehicle
   * @param sideDistance x-offset for sensor
   * @param aheadDistance y-offset for sensor
   */
  VehicleShell(PVector pos, float mass, float sideDistance, float aheadDistance) {
    this.pos = pos; 
    this.mass = mass; 
    this.sideDistance = sideDistance; 
    this.aheadDistance = aheadDistance; 
    
    vWidth = sideDistance*2; 
    vHeight = aheadDistance*2; 
    
    createBody();
  }
  
  /**
   * Initialize and position PShapes that form car's body
   */
  void createBody() {
    body = createShape(RECT,0,0,vWidth,vHeight); 
    body.setFill(color(255,0,0)); 
    
    sensorL = createShape(ELLIPSE,0-(sideDistance+OFFSET),0-(aheadDistance+OFFSET),DIAM,DIAM);
    sensorR = createShape(ELLIPSE,sideDistance+OFFSET,-aheadDistance-OFFSET,DIAM,DIAM); 
    sensorL.setFill(color(0,255,0)); // green
    sensorR.setFill(color(0,0,255)); // blue
  }
  
  /**
   * Display car to screen
   */
  void display() {
    pushMatrix(); 
   
    translate(pos.x, pos.y); 
    shapeMode(CENTER); 
    shape(body); 
    shape(sensorL); 
    shape(sensorR); 
    
    popMatrix();  
  }
  
  void updateSensorPositions() { } 
  void getLightInput() { }
  void calcWheelVelocity() { }
  void updateVehicle() { }
}
