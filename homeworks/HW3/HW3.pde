/**
 * HW #3: Asteroids
 * IGME-202-02
 * 
 * @author Veronica Wharton (vlw1338)
 */

/** 
 * Main class for an 'Asteroids' game
 */
 
import java.util.Iterator;

ArrayList<Asteroid> asteroids;      // array of asteroids
int NUM_ASTEROIDS;                  // max number of asteroids
Ship ship;                          // the ship

int score = 0; 

/**
 * Setup method. Called once at beginning of process.
 */
public void setup() {
  size(500,700,P2D);
  
  // instantiate asteroids
  NUM_ASTEROIDS = int(random(10,21)); 
  asteroids = new ArrayList<Asteroid>(); 
  for (int i = 0; i < NUM_ASTEROIDS; i++) {
     asteroids.add(new Asteroid()); 
  }
  
  // instantiate ship
  ship = new Ship(new PVector(width/2, height/2)); 
}

/**
 * Draw method. Called once per frame.
 */
public void draw() {
  // redraw background at beginning of each frame
  background(0);
  
  // check for collisions between asteroids and bullets
  // and between asteroids and ship
  checkForCollisions();
  
  // draw ship to screen
  // (movement is handled by Ship class)
  ship.render(); 
 
  // draw asteroids to screen
  // (movement is handled by Asteroid class)
  for (int i = 0; i < asteroids.size(); i++) {
    asteroids.get(i).render(); 
  }
  
  // display score
  text("score: " + score, 10, 15); 
  
  // add new asteroids to the screen if there
  // are less than a third of the original number
  // left
  if (asteroids.size() < NUM_ASTEROIDS/3) {
    for (int i = 0; i < NUM_ASTEROIDS; i++) {
       asteroids.add(new Asteroid()); 
    }
    
    // increase the max number of asteroids that can 
    // be on the screen (making the game harder)
    NUM_ASTEROIDS += 2; 
  }
}

/**
 * Calls ship's keyPressed method (input analysis
 * is handled internally by the Ship class)
 */
public void keyPressed() {
  if (key == 'a' || key == 'A') {
    saveFrame();
  }
  else {
    ship.shipKeyPressed(key, keyCode);
  }  
}

/**
 * Calls ship's keyReleased method (input analysis
 * is handled internally by the Ship class)
 */
public void keyReleased() {
  if (key != 'a' || key != 'A') {
    ship.shipKeyReleased(key, keyCode);
  }  
}

/**
 * Checks for collisions between all asteroids and all bullets
 * and between all asteroids and the ship
 */
public void checkForCollisions() {
  
  // iterate through all asteroids
  Iterator<Asteroid> i = asteroids.iterator(); 
  while (i.hasNext()) {
    Asteroid a = i.next();
    
    float distanceBetween = PVector.dist(a.pos(), ship.pos()); 
    float radiusSum = a.diameter()/2 + ship.size/2; 
    
    // if ship and asteroid collide, ship's stroke changes to a random color
    if (distanceBetween <= radiusSum) {
      ship.ship.setStroke(color(random(-100,500),random(-100,500),random(-100,500)));  
    }
   
    // iteratre through all bullets 
    Iterator<Bullet> j = ship.bulletIterator(); 
    while (j.hasNext()) {
      Bullet b = j.next();
     
      // bounding circle collision detection 
      distanceBetween = PVector.dist(a.pos(), b.pos()); 
      radiusSum = a.diameter()/2. + b.diameter()/2.;
      
      // a bullet has hit an asteroid
      if (distanceBetween <= radiusSum) {
        i.remove(); // remove asteroid
        j.remove(); // remove bullet
        score++; 
      }  
    }
  }
}

/**
 * Ensures that a given position vector stays within
 * the Processing window.  Ideally, the position vector
 * represents an object that is roughly circular.
 * 
 * @param pos The position vector to constrain
 * @param size The radius of the object (creates more realistic
 * wraparound effect)
 */
public void wraparound(PVector pos, int size) {
    if (pos.x > width+size) pos.x = -size;  
    else if (pos.x < -size) pos.x = width+size;
    if (pos.y > height+size) pos.y = -size; 
    else if (pos.y < -size) pos.y = height+size; 
  }
