/**
 * HW #3: Asteroids
 * IGME-202-02
 * 
 * @author Veronica Wharton (vlw1338)
 */

/**
 * A class that represents a bullet shot from a spaceship.
 */
public class Bullet {
  PVector pos;       // bullet's current position
  float angle;       // angle at which bullet is traveling
  float speed;       // speed at which bullet is traveling
  int age;           // number of frames since bullet was launched
 
  PShape bullet;     // visualization of bullet
  int diameter = 2;  // diameter of bullet
  
  // NOTE: bullets do not experience friction
  
  /**
   * Constructor for a bullet
   * 
   * @param pos The bullet's starting position
   * @param angle The angle at which the bullet should travel
   * @param speed The speed at which the bullet should travel
   */
  public Bullet(PVector pos, float angle, float speed) {
     this.pos = pos; 
     this.angle = angle; 
     this.speed = speed; 
     
     // create visualization of bullet
     bullet = createShape(ELLIPSE, 0, 0, diameter, diameter);
     bullet.setFill(color(255));        // white fill
     bullet.setStroke(color(255));      // white stroke
  }
  
  /**
   * Update the bullet's position, and
   * draw the bullet to the screen
   */
  public void render() {
    // update position
    pos.x += (speed * cos(angle)); 
    pos.y += (speed * sin(angle));
    wraparound(pos, diameter/2); 
    
    // draw bullet to screen
    pushMatrix(); 
    translate(pos.x, pos.y);
    rotate(angle);  
    shape(bullet, 0, 0); 
    popMatrix(); 
    
    // increment age
    age++;  
  }
  
  /**
   * Getter for bullet's age
   * 
   * @return The bullet's age
   */
  public int age() {
    return age;  
  }
  
  /**
   * Getter for bullet's current position
   * 
   * @return The bullet's current position
   */
  public PVector pos() {
    return pos;  
  }
  
  /**
   * Getter for bullet's diameter
   * 
   * @return The bullet's diameter
   */
  public int diameter() {
    return diameter;  
  }
}
