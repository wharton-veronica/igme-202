/** 
 * IGME-202-02
 * Project #1: Braitenberg Vehicles (2D)
 * 
 * @author Veronica Wharton (vlw1338)
 */
 
// list of vehicles
ArrayList<Vehicle> vehicles; 

// list of light sources
ArrayList<Light> lights; 

void setup() {
  size(800,600,P2D);
 
  vehicles = new ArrayList<Vehicle>(); 
  lights = new ArrayList<Light>(); 
  
  // create vehicles
  int NUM_VEHICLES = 10; 
  for (int i = 0; i < NUM_VEHICLES; i++) {
    
    // alterate between creating cowardly and aggressive vehicles
    boolean coward = true; 
    if (i % 2 == 0) {
      coward = false;  
    }
    
    // create vehicles
    vehicles.add(new Vehicle(
      new PVector(random(100,500), random(100,500)), // starting location
      2.5, // mass
      9.,  // side distance
      14., // ahead distance
      coward // whether the vehicle is cowardly or aggressive
    )); 
  }
  
  // create lights
  int NUM_LIGHTS = 40; 
  for (int i = 0; i < NUM_LIGHTS; i++) {
    lights.add(new Light(
      new PVector(random(0,width), random(0,height)), // starting location
      int(random(10,30)) // brightness
    )); 
  } 
    
}

void draw() {
  background(50); 
  
  // iterate through all vehicles
  for (Vehicle v : vehicles) {
    
    // update the vehicle's internal state
    v.update(); 
   
    // display vehicle to screen
    v.display(); 
  }
  
  // iterate through all lights and display them
  for (Light l : lights) {
    l.display();  
  }
}

/**
 * Called when mouse button is pressed
 */
void mousePressed() {
  
  // iterate through all lights
  for (Light l : lights) {
    
    // implement draggable light sources - 
    // test for collision between mouse's current location and 
    // each of the lights
    l.mousePressed();  
  } 
}

/**
 * Called when mouse button is released
 */
void mouseReleased() {
  
  // iterate through all lights
  for (Light l : lights) {
    
    // release any/all lights from being dragged by the mouse
    l.mouseReleased();  
  } 
}

void keyPressed() {
  if (key == ' ') {
    for (Vehicle v : vehicles) {
      v.showTrail = !v.showTrail;   
    } 
  } 
}
