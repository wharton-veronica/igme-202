class Rod {
  int rodLength = 170, rodWidth = 20; 
  PShape rod;
 
  PVector center;
  float angle;  
  
  int state; 
  // 0 - rod's starting position has not been set yet
  // 1 - rod is released 
  
  Rod(PVector center) {
    state = 0; 
    this.center = center; 
    rod = createShape(RECT, 0, 0, rodLength, rodWidth); 
  }
 
  void setEnd(PVector end) {
    angle = atan2(end.y-center.y, end.x-center.y); 
  } 
  
  void render() {
     pushMatrix(); 
     translate(center.x, center.y); 
     rotate(angle); 
     shape(rod, 0, -rodWidth/2); 
     popMatrix(); 
  }
  
  void updateState() {
    if (state == 0) {
      state++;  
    }
  }
}
