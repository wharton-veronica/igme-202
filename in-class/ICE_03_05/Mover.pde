
class Mover{
  
    //class variables
    PVector position;
    PVector direction; 
    PVector velocity;
    PVector acceleration; 
    int speed;
    
    //Constructor
    Mover(int s){
        position = new PVector(400, 400);
        direction = new PVector(0,0); 
        velocity = new PVector(0,0);
        speed = s;
    }
    
    void updatePosition(){
        direction.x = direction.y = 0; 
        if(isMovingRight == true){
            direction.x = 1; 
            direction.y = 0; 
        }
        if(isMovingLeft == true){
            direction.x = -1; 
            direction.y = 0; 
        }
        if(isMovingUp == true){
            direction.x = 0; 
            direction.y = -1; 
        }
        if(isMovingDown == true){
            direction.x = 0; 
            direction.y = 1; 
        }
          
        velocity = PVector.mult(direction, speed); 
        position.add(velocity); 
        
        //Another way of writing one-line if statements
        if(position.x > width) position.x = 0;
        if(position.x < 0) position.x = width;
        if(position.y > height) position.y = 0;
        if(position.y < 0) position.y = width;
    }
    
    void display(){
        ellipse(position.x, position.y, 20, 20); 
    }
}
