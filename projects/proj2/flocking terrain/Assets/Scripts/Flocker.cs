﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Steer))]
[RequireComponent(typeof(CharacterController))]


public class Flocker : MonoBehaviour {

	// movement variables - exposed in inspector panel
	private List<GameObject> planets = null; 
	private static int currentPlanetIndex = 0; 
	public static GameObject currentPlanet = null;
	public GameManager gm;

	// Each vehicle contains a CharacterController which helps to deal with
	// the relationship between movement initiated by the character and the forces
	// generated by contact with the terrain & other game objects.
	private CharacterController characterController;
	
	// the SteeringAttributes holds several variables needed for steering
	private SteeringAttributes attr;

	// the Steer component implements the basic steering functions
	private Steer steer;

	private Vector3 acceleration;	// change in velocity per second
	private Vector3 velocity;		// change in position per second
	public Vector3 Velocity {
		get { return velocity; }
		set { velocity = value;}
	}

	public List<GameObject> Planets {
		get { return planets; }
		set { planets = value; }
	}
	
	void Start() {
		acceleration = Vector3.zero;
		velocity = transform.forward;

		// get component references
		characterController = gameObject.GetComponent<CharacterController> ();
		steer = gameObject.GetComponent<Steer> ();
		attr = GameObject.Find("MainGO").GetComponent<SteeringAttributes> ();
		gm = GameObject.Find("MainGO").GetComponent<GameManager>();
	}
	
	void Update() {
		CalcSteeringForce();
		
		// update velocity
		velocity += acceleration * Time.deltaTime;
		velocity = Vector3.ClampMagnitude (velocity, attr.maxSpeed);
		
		// orient the transform to face where we going
		if (velocity != Vector3.zero) {
			transform.forward = velocity.normalized;
		}

		// the CharacterController moves us subject to physical constraints
		characterController.Move (velocity * Time.deltaTime);
		
		// reset acceleration for next cycle
		acceleration = Vector3.zero;
	}
	
	
	// calculate and apply steering forces
	private void CalcSteeringForce() { 
		Vector3 force = Vector3.zero;
		GameObject target = currentPlanet;

		// avoid all planets (except the target) 
		for ( int i = 0; i < planets.Count; i++ )
		{	
			if ( planets[i] != target ) {
				force += attr.avoidWt * steer.AvoidObstacle ( planets[i], /*attr.avoidDist*/planets[i].GetComponent<Planet>().Radius*7 );
			}
		}
		force += attr.avoidWt * steer.AvoidObstacle ( gm.Sun, 300f ); 
	 
		// in bounds
		force += attr.inBoundsWt * steer.StayInBounds( 480, new Vector3(250, 10, 250) );
		
		// seek target (a planet)
		if ( target ) {

			double radius = target.GetComponent<Planet>().Radius; 

			Vector3 arriveForce = attr.seekWt * steer.Arrive ( target.transform.position, (float)radius*9 );

			force += arriveForce;

			// if ship has arrived at planet, update the target planet
			if ( Vector3.Distance( transform.position, target.transform.position ) < radius*6 ) {
				currentPlanetIndex++; 
				currentPlanetIndex %= planets.Count; 
				currentPlanet = planets[currentPlanetIndex]; 
			}
		}
		
		// separation
		force += attr.separationWt * steer.Separate( gm.Flock );
					
		// alignment
		force += attr.alignmentWt * steer.Align( gm.FlockDirection );

		// cohesion
		force += attr.cohesionWt * steer.Cohere( gm.Centroid );

		// limit force
		force = Vector3.ClampMagnitude( force, attr.maxForce );

		ApplyForce( force );
	}

	
	private void ApplyForce (Vector3 steeringForce) {
		acceleration += steeringForce/attr.mass;
	}
}
