
//TRANSFORMATIONS IN-CLASS EXERCISE
//WRITTEN BY ERIN CASCIOLI FOR IGME 202
//SECTION 2, MWF 12 - 12:50

//COMPLETED BY VERONICA WHARTON (VLW1338)

// CAR CLASS
class Car{
  
  // GLOBAL X AND Y VARIABLES
  // DECLARE PSHAPE
  int x;
  int y;
  float angle; 
  int scale;
  float scaleIndex = 0;  
  PShape car;
  
  // CREATE CONSTRUCTOR
  Car(int x_, int y_){
    // ASSIGN PARAMETERS TO GLOBAL VARIABLES HERE
    x = x_; 
    y = y_;
    angle = 0; 
    scale = 1;  
    
    // CREATE CAR PSHAPE HERE
    // CREATE YOUR OWN VERTICES (OR USE THE ONES BELOW)
    car = createShape();
    car.beginShape();
      car.vertex(0, 10);
      car.vertex(20, 10);
      car.vertex(30, 0);
      car.vertex(50, 0);
      car.vertex(60, 10);
      car.vertex(80, 10);
      car.vertex(80, 30);
      car.vertex(0, 30);
    car.endShape(CLOSE);
  }
  
  // DISPLAY WILL DRAW THIS CAR TO THE WINDOW
  // APPLY TRANSLATION HERE
  // THIS CODE IS ALREADY DONE FOR YOU
  void displayTranslated(){
    pushMatrix();
    
    translate(x+=int(random(-5,5)),y+=int(random(-5,5)));
    rotate(angle); 
    scale(scale);
    
    shape(car);
    popMatrix();
  }
  
  // DISPLAY WILL DRAW THIS CAR TO THE WINDOW
  // APPLY ROTATIONS HERE
  // COMPLETE THE FUNCTION BELOW
  void displayRotated(){
    pushMatrix();
    
    translate(x,y);  
    rotate(angle+=(random(-PI/24,PI/24)));
    scale(scale); 
    
    shape(car); 
    popMatrix(); 
  }
  
  // DISPLAY WILL DRAW THIS CAR TO THE WINDOW
  // APPLY SCALING HERE
  // COMPLETE THE FUNCTION BELOW
  void displayScaled(){
    pushMatrix(); 
    
    translate(x,y);
    rotate(angle);
    scale(scale); 
    scale+=random(-2,1); 
    if (scale <= 1) {
      scale = 1;  
    }
    scale(scale);
    /*if (random(1) < 0.5) {
      scale(scale+=0.001);
    } 
    else {
      scale(scale-=0.001);
    } */
    
    
    shape(car); 
    popMatrix(); 
  }
  
}
