class Mover {
  PVector pos; 
  int speed; 
  PShape mover;
  int d = 20; 
 
  boolean right, left, up, down;  
 
  Mover( PVector pos, int s) {
    this.pos = pos; 
    this.speed = s; 
    mover = createShape(ELLIPSE, 0, 0, d, d); 
  } 
  
  void move() {
     if (right) {
       pos.x += speed;
       if (pos.x > width+d/2)
         pos.x = 0; 
     }
     if (left) {
       pos.x -= speed;
       if (pos.x < -d/2)
         pos.x = width; 
     }
     if (up) {
       pos.y -= speed;
       if (pos.y < -d/2)
         pos.y = height;
     }
     if (down) {
       pos.y += speed;
       if (pos.y > height+d/2)
         pos.y = 0;
     }
  }
  
  void render() {
    move(); 
    mover.setFill(color(
      int(random(-100,500)),
      int(random(-100,500)),
      int(random(-100,500))
      )); 
    shapeMode(CENTER); 
    shape(mover, pos.x, pos.y); 
  }
}
