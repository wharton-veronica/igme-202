/**
 * Veronica Wharton
 * IGME-202-02
 * HW #2: Rolling through Random
 */

class Grass {
  PShape noise; 
  float index;
  color c; 
  float bottom; 
  
  final float increment = 0.7; 
  
  Grass(color c, float bottom) {
    this(0.00, c, bottom); 
  }
  
  Grass(float seed, color c, float bottom) {
    this.index = seed;
    this.c = c;
    this.bottom = bottom;   
  }
  
  void render() {
     noise = createShape(); 
     noise.setFill(c); 
     noise.setStroke(false); 
     noise.beginShape();
       noise.vertex(width,height); 
       noise.vertex(0,height); 
       
       float localIndex = 0.0; 
       for (int i = 0; i <= width; i++) {
          float x = i; 
          float y = bottom + noise(index+localIndex)*50; 
          noise.vertex(x, y); 
          localIndex += increment; 
       }
     noise.endShape(); 
     
     shapeMode(CORNER);
     shape(noise); 
     
     index += increment; 
  }
  
}
