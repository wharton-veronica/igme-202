class Box {
  int minX, maxX, minY, maxY;
  PShape box; 
  int WIDTH = 100, HEIGHT = 100;  
 
  Box() {
    box = createShape(RECT,0,0,WIDTH,HEIGHT); 
  }
 
  void render(int x, int y) {
    minX = x-WIDTH/2;
    maxX = x+WIDTH/2;
    minY = y-HEIGHT/2; 
    maxY = y+HEIGHT/2; 
    shape(box, x, y); 
  } 
  
  void setFill(color c) {
    box.setFill(c);  
  }
}
