using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	GameObject myGuy;
	GameObject targ;
	
	public GameObject TargetPrefab;
	public GameObject GuyPrefab;
	public GameObject ObstaclePrefab;
	
	// Use this for initialization
	void Start () {
		Vector3 pos = new Vector3(Random.Range(-40, 40), 4f, Random.Range( -40, 40));
		targ = (GameObject)GameObject.Instantiate(TargetPrefab, pos, Quaternion.identity);
		
		pos = new Vector3(0, 1.0f, 0);
		myGuy = (GameObject)GameObject.Instantiate(GuyPrefab, pos, Quaternion.identity);
		myGuy.GetComponent<SteeringVehicle>().Target = targ.gameObject;
		
		//make some obstacles
		for (int i=0; i<30; i++) 
		{
			pos =  new Vector3(Random.Range(-40, 40), 4f, Random.Range(-40, 40));
			Quaternion rot = Quaternion.Euler(0, Random.Range(0, 90), 0);
			GameObject o = (GameObject)GameObject.Instantiate(ObstaclePrefab, pos, rot);

			//cubes are dynamically created with a width somewhere between 2 and 5
			//float scal = Random.Range(2f, 5f);
			float scal = Random.Range (2f, 5f);
			o.transform.localScale = new Vector3(scal, scal, scal);
		}

		//some things we need to do flocking:
		//variables for centroid
		//direction
		//ArrayList flockers
		//accessors for these variables
		//we need to update the centroid and direction variables every frame
		//we can make sure this happens before flockers update by having the flockers use 'late update' instead of 'update'
		//this makes sure the game manager figured this all out before I try to do the steering
		
		//tell camera to follow myGuy
		Camera.main.GetComponent<SmoothFollow>().target = myGuy.transform;
		
	}
	
	// Update is called once per frame
	void Update () {

		// if myGuy is less than 5 units away from the target, move the target to a new random location
		if(Vector3.Distance( myGuy.transform.position, targ.transform.position) < 5) 
		{
			targ.transform.position = new Vector3(Random.Range(-30, 30), 4f, Random.Range(-30, 30));
		}
	}
}

