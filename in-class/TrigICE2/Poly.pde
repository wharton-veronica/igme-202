/* 
 * Veronica Wharton
 * IGME-202-02
 * ICE: Trig, PShape, and Polygons
 */

class Poly {
  int numSides, xOffset, yOffset;
  float radius; 
  PShape poly; 
  
  Poly (int numSides, float radius, int xOffset, int yOffset) {
     this.numSides = numSides;
     this.radius = radius; 
     this.xOffset = xOffset; 
     this.yOffset = yOffset;
   
   poly = createShape(); 
   poly.beginShape(); 
   
   // create vertices using trig
   for (float i = 0; i < 2*PI; i += (2*PI)/numSides) {
      println(yOffset+radius*sin(i)); 
      poly.vertex(xOffset+radius*cos(i), yOffset+radius*sin(i));  
   } 
 
   poly.endShape(CLOSE);   
  }
  
  void display() {
    shape(poly); 
  }
}
