//TRANSFORMATIONS IN-CLASS EXERCISE
//WRITTEN BY ERIN CASCIOLI FOR IGME 202
//SECTION 2, MWF 12 - 12:50

//COMPLETED BY VERONICA WHARTON (VLW1338)

// DECLARE SOME CAR OBJECTS HERE (MINIMUM OF 10)
Car[] cars; 
int NUM_CARS = 10; 

// SET WINDOW SIZE
// SET BACKGROUND COLOR
// CONSTRUCT SOME CAR OBJECTS
void setup(){
  size(500,500,P2D); 
  cars = new Car[NUM_CARS]; 
  for (int i = 0; i < cars.length; i++) {
    cars[i] = new Car(int(random(width)),int(random(height)));  
  }
  frameRate(30); 
  shapeMode(CENTER); 
}


// DISPLAY YOUR CAR OBJECTS HERE
// CALL DISPLAYTRANSLATED(), DISPLAYROTATED(), OR DISPLAYSCALED() ON VARIOUS CARS
// EX:  CAR1.DISPLAYTRANSLATED(); CAR2.DISPLAYROTATED(); CAR3.DISPLAYSCALED();
void draw(){
  background(50);
  for (int i = 0; i < cars.length; i++) {
    switch(int(random(0,3))) {
      case 0: cars[i].displayTranslated(); break; 
      case 1: cars[i].displayRotated(); break; 
      case 2: cars[i].displayScaled(); break;
    }
     
  }
  
}

