class Vehicle {
  PVector pos; 
  
  PShape vehicle, body, leftSensor, rightSensor, leftWheel, rightWheel; 
  PVector leftSensorPos, rightSensorPos; 
  
  Vehicle(PVector pos) {
    this.pos = pos; 
    body = createShape(RECT, 0, 0, 10, 20); 
    body.setFill(color(50)); 
    leftSensor = createShape(RECT, -5, 10, 5, 5); 
    leftSensor.setFill(color(255,0,0)); 
    rightSensor = createShape(RECT, 5, 10, 5, 5);
    rightSensor.setFill(color(255,0,0)); 
    /*leftWheel = createShape(RECT, -5, -10, 2, 5); 
    leftWheel.setFill(color(0));
    rightWheel = createShape(RECT, 5, -10, 2, 5); 
    rightWheel.setFill(color(0)); */ 
    
    vehicle = createShape(GROUP); 
    vehicle.addChild(body); 
    vehicle.addChild(leftSensor); 
    vehicle.addChild(rightSensor); 
    /*vehicle.addChild(leftWheel); 
    vehicle.addChild(rightWheel); */
  }
 
  void render() {
    shapeMode(CENTER); 
    pushMatrix(); 
    translate(pos.x, pos.y); 
    shape(vehicle, 0, 0);
    ellipseMode(CENTER);  
    ellipse(0,0,5,5); 
    popMatrix();  
  }
}
