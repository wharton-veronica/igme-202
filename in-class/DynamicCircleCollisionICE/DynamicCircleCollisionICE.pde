//---------------------------------------
// Bounding Circle ICE
// Written by Erin Cascioli
// IGME 202
//
// @author Veronica Wharton (vlw1338)
//---------------------------------------

float radiusSum, distanceBetween;

ArrayList<Circle> hitCircles; 
Circle mouseCircle;
final int NUM_CIRCLES = 500; 

void setup(){
    // window size: 500 x 500
    size(500,500,P2D); 

    // create static hit circles
    hitCircles = new ArrayList<Circle>(); 
    for (int i = 0; i < NUM_CIRCLES; i++) {
      hitCircles.add(new Circle(
        int(random(15,30)),       // radius of circle
        randomColor(),            // color of circle
        randomPositionOnScreen()  // coordinates of circle's center
      ));  
    }
    
    // create moving circle
    mouseCircle = new Circle(
      30, 
      randomColor()
    ); 
    
    // set the sum of the radii
    //radiusSum = hitCircle.radius() + mouseCircle.radius(); 
}


void draw(){
    // redraw background at the beginning of each frame
    background(127);
   
    // update circle centers
    mouseCircle.setCenter(new PVector(mouseX,mouseY));
    
    
    // calculate difference between two circles
    
    for (int i = 0; i < hitCircles.size(); i++) {
       radiusSum = hitCircles.get(i).radius() + mouseCircle.radius();
       distanceBetween = checkDistance(hitCircles.get(i),mouseCircle);
       if (distanceBetween < radiusSum) {
           hitCircles.get(i).setFill(#000000); 
        }
      else {
          hitCircles.get(i).setDefaultFill(); 
      }
    }
    
    // draw circles to window
    for (int i = 0; i < hitCircles.size(); i++) {
      hitCircles.get(i).render(); 
    } 
    mouseCircle.render();
}

/**
 * Calculates the distance between 2 Circles
 * @param c1 The first Circle
 * @param c2 The second Circle
 * @return The distance between the centers of the two circles
 */
float checkDistance(Circle c1, Circle c2){
  return dist(c1.center().x, c1.center().y, c2.center().x, c2.center().y);
}

color randomColor() {
  int r = int(random(-100,500));
  int g = int(random(-100,500)); 
  int b = int(random(-100,500)); 
  return color(r,g,b);
}

PVector randomPositionOnScreen() {
  int x = int(random(0,width));
  int y = int(random(0,height));
  return new PVector(x,y);
}


