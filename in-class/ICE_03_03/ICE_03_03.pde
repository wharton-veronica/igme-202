Mover mover; 

void setup() {
  size(500,500,P2D); 
  mover = new Mover(
    new PVector(width/2, height/2),
    5
    ); 
   
}

void draw() {
  mover.render();  
}

void keyPressed() {
  if (keyCode == RIGHT) {
    mover.right = true; 
  }
  if (keyCode == LEFT) {
    mover.left = true; 
  }
  if (keyCode == UP) {
    mover.up = true; 
  }
  if (keyCode == DOWN) {
    mover.down = true; 
  }
   
}

void keyReleased() {
  if (keyCode == RIGHT) {
    mover.right = false; 
  }
  if (keyCode == LEFT) {
    mover.left = false; 
  }
  if (keyCode == UP) {
    mover.up = false; 
  }
  if (keyCode == DOWN) {
    mover.down = false; 
  }  
}
