//---------------------------------------
// Bounding Circle ICE
// Written by Erin Cascioli
// IGME 202
//
// @author Veronica Wharton (vlw1338)
//---------------------------------------

float radiusSum, distanceBetween;

Circle mouseCircle, hitCircle; 

void setup(){
    // window size: 500 x 500
    // background: 127,127,127
    size(500,500,P2D); 

    // create static hit circle
    hitCircle = new Circle(
      50, 
      color(#ffffff)
    ); 
    
    // create moving circle
    mouseCircle = new Circle(
      30, 
      color(#ff0000)
    ); 
    
    // set the sum of the radii
    radiusSum = hitCircle.radius() + mouseCircle.radius(); 
}


void draw(){
    // redraw background at the beginning of each frame
    background(127);
   
    // update circle centers
    hitCircle.setCenter(new PVector(width/2,height/2));
    mouseCircle.setCenter(new PVector(mouseX,mouseY));
    
    // calculate difference between two circles
    distanceBetween = checkDistance(hitCircle,mouseCircle);
    if (distanceBetween < radiusSum) {
      mouseCircle.setFill(#00ff00); 
    }
    else {
      mouseCircle.setDefaultFill(); 
    }
    
    // draw circles to window
    hitCircle.render(); 
    mouseCircle.render(); 
}

/**
 * Calculates the distance between 2 Circles
 * @param c1 The first Circle
 * @param c2 The second Circle
 * @return The distance between the centers of the two circles
 */
float checkDistance(Circle c1, Circle c2){
  return dist(c1.center().x, c1.center().y, c2.center().x, c2.center().y);
}


