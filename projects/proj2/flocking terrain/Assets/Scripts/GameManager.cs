﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	private List<GameObject> flock;
	public List<GameObject> Flock { get{ return flock; } }

	private List<GameObject> planets; 
	private List<GameObject> Planets { get{ return planets; } }
	
	// these variable are visible in the Inspector
	public GameObject TargetPrefab;
	public GameObject GuyPrefab;
	public GameObject ObstaclePrefab;
	public GameObject PlanetPrefab; 
	public GameObject Sun; 
	
	private Vector3 flockDirection;
	public Vector3 FlockDirection {
		get { return flockDirection; }
	}
	private Vector3 centroid;
	public Vector3 Centroid { get { return centroid; } }

	// Use this for initialization
	void Start () {

		Vector3 pos;
		flock = new List<GameObject>();
		planets = new List<GameObject>(); 

		// make some planets
		int NUM_PLANETS = 10; 
		for ( int i = 0; i < NUM_PLANETS; i++) {
			
			// resize the planets 
			float scal = Random.Range(6f, 15f);

			pos =  new Vector3 ( Random.Range( -400, 400 ), Random.Range( -400, 400 ) , Random.Range( -400, 400 ) );
			Quaternion rot = Quaternion.Euler(0, Random.Range(0, 90), 0);
			GameObject planet = (GameObject)GameObject.Instantiate(PlanetPrefab, pos, rot);
			planet.GetComponent<Planet>().Radius = scal / 2;
			planet.GetComponent<ObstacleScript>().Radius = scal / 2; 
			planets.Add( planet ); 
			
			planet.transform.localScale = new Vector3(scal, scal, scal);
		}
		Flocker.currentPlanet = planets[0]; 

		// make flock
		int NUM_SHIPS = 4; 
		for (int i = 0; i < NUM_SHIPS; i++) 
		{
			pos =  new Vector3( Random.Range( 200, 300 ), 0f, Random.Range( 200, 300 ) );
			GameObject guy = (GameObject)GameObject.Instantiate(GuyPrefab, pos, Quaternion.identity);
			guy.GetComponent<Flocker>().Planets = planets; 
			flock.Add(guy);

			float scal = 0.05f; 
			guy.transform.localScale = new Vector3(scal, scal, scal); 
		}	
	}
	
	// Update is called once per frame
	void Update () {

		// find average position of each flocker
		calcCentroid(); 

		// find average "forward" for each flocker
		calcFlockDirection();
		
	}
	
	
	private void calcCentroid()
	{
		centroid = Vector3.zero;
		foreach (GameObject f in Flock)
		{
			centroid += f.transform.position;
		}
		centroid /= Flock.Count;
		gameObject.transform.position = centroid;
	}
	
	private void calcFlockDirection()
	{
		flockDirection = Vector3.zero;
		foreach (GameObject f in Flock)
		{
			flockDirection += f.transform.forward;
		}
		flockDirection /= Flock.Count;
		gameObject.transform.forward = flockDirection;
	}
}
