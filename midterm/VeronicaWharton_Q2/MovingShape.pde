/**
 * Midterm Practical Exam: Question #2
 * IGME-202-02
 * Veronica Wharton
 */

class MovingShape {
  
  PVector velocity, position;  // vectors for shape's velocity and position
  float angle;                // shape's current angle of rotation [0, 2PI)
  int acceleration;           // scalar quantity for shape's acceleration
  PShape squareGroup;         // group of PShapes
  PShape square1, square2, square3; 
  
  /**
   * Constructor for a moving shape
   * 
   * @param accel Scalar quantity representing the shape's acceleration
   */
  MovingShape(int accel) {
    // initialize velocity and position vectors
    velocity = new PVector(0,0);
    position = new PVector(width/2, height/2);
    
    // initialize angle of rotation
    angle = 0;
  
    // store acceleration scalar for later use  
    acceleration = accel; 
   
    // create component shapes
    // color each component appropriately
    square1 = createShape(RECT, 0, 0, 20, 20); 
    square1.setFill(color(255,0,0)); 
    square2 = createShape(RECT, 20, 20, 20, 20); 
    square2.setFill(color(0,255,0)); 
    square3 = createShape(ELLIPSE, 20, 0, 20, 20); 
    square3.setFill(color(0,0,255)); 
    
    // add component shapes to a shape group for easier rendering
    squareGroup = createShape(GROUP); 
    squareGroup.addChild(square1); 
    squareGroup.addChild(square2); 
    squareGroup.addChild(square3);
  }
  
  /**
   * Update the shape's position
   */
  void update() {
    // if left mouse button is pressed, 
    // rotate the shape 20 degrees CW
    if (turning) {
      angle += radians(20); 
    }  
    
    // calculate acceleration vector
    float accelMag = acceleration; 
    PVector acceleration = new PVector(accelMag*cos(angle), accelMag*sin(angle)); 
    
    // calclulate new velocity vector
    float velocMag = velocity.mag(); 
    velocity = new PVector(velocMag*cos(angle), velocMag*sin(angle)); 
    
    // add acceleration to velocity
    velocity.add(acceleration); 
    
    // implement terminal velocity
    velocity.limit(5);
    
    // add velocity to position
    position.add(velocity); 
    
    // wrap shape's position around edges of screen
    if (position.x > width+20) {
      position.x = -20;  
    }
    else if (position.x < -20) {
      position.x = width+20; 
    }
    if (position.y > height+20) {
      position.y = -20;  
    }
    else if (position.y < -20) {
      position.y = height+20; 
    }
  }
  
  /**
   * Display shape to screen
   */
  void display() {
    pushMatrix(); 
    translate(position.x, position.y); 
    rotate(angle); 
    shape(squareGroup, -20, -20); 
    popMatrix(); 
  }
}
