using UnityEngine;
using System.Collections;

public class SteeringAttributes : MonoBehaviour {
	
	//these are common attributes required for steering calculations 
	public float maxSpeed;
	public float maxForce;
	public float mass;
	public float radius;
	

	void Start ()
	{
		maxForce = 12.0f;
		maxSpeed = 12.0f;
		mass = 1.0f;
		radius = 1.0f;
	}
}