class Walker {
  int x, y;  
  
  Walker(int x_, int y_) {
    x = x_; 
    y = y_; 
  } 
  
  void move() {
    
    /* uniform randomness */
    /*// [-2,-1) -> -1
    // [-1, 0) -> 0
    // [0, 1)  -> 0
    // [1, 2)  -> 1
    x += int(random(-2,2)); 
    y += int(random(-2,2)); */
    
    /* directed randomness */
    float r = random(1); 
    if (r < 0.4) 
      x++;
    else if (r < 0.6)
      x--;
    else if (r < 0.8)
      y++;
    else 
      y--;
      
    // draw dot
    rectMode(CENTER); 
    rect(x,y,1,1);  
  }
}
