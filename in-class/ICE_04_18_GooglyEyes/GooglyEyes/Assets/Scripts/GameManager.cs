﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject BlockPrefab;
	public GameObject GuyPrefab; 
	private GameObject myGuy; 

	// Use this for initialization
	void Start () {
		int NUM_BLOCKS = 3; 
		for (int i = 0; i < NUM_BLOCKS; i++) {
			Vector3 pos = new Vector3(
				Random.Range(-5,5), 
				1f, 
				Random.Range(-5,5)); 
			Quaternion rot = Quaternion.Euler(
				0, 
				Random.Range(0,90), 
				0);
			Instantiate( BlockPrefab, pos, rot ); 
		}

		myGuy = (GameObject) Instantiate( GuyPrefab, new Vector3( 0f, 1f, 0f ), Quaternion.identity ); 
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
