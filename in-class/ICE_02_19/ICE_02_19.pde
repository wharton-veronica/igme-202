// global variables

Box boxA, boxB; 

void setup() {
  size(500,500,P2D); 
  boxA = new Box(); 
  boxB = new Box(); 
  shapeMode(CENTER); 
}

void draw() {
  background(127); 
  
  checkBoxCollision();

  boxA.render(width/2,height/2); 
  boxB.render(mouseX,mouseY);  
}

void checkBoxCollision() {
  if (boxA.minX < boxB.maxX && boxA.maxX > boxB.minX &&
      boxA.minY < boxB.maxY && boxA.maxY > boxB.minY) {
      boxB.setFill(color(0, 0, 255));  
  }
 else {
      boxB.setFill(color(0,0,0));
 } 
}
