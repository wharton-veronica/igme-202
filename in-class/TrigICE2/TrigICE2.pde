/* 
 * Veronica Wharton
 * IGME-202-02
 * ICE: Trig, PShape, and Polygons
 */

Poly poly; 

void setup() {
  size(500,500,P2D);
  background(255,0,0); 
  poly = new Poly(3,70.0,width/2,height/2); 
}

void draw() {
  poly.display();  
}
