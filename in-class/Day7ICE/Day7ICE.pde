Bubble bubble; 
Bubble[] bubbles; 

void setup() {
  size(500,500,P2D);
  bubble = new Bubble(new PVector(0,0)); 
  bubbles = new Bubble[100]; 
  for (int i = 0; i < bubbles.length; i++) {
    bubbles[i] = new Bubble(new PVector(random(width),random(height))); 
  } 
}

void draw() {
  background(50); 
  for (int i = 0; i < bubbles.length; i++) {
    bubbles[i].render();  
  }
  //bubble2.render(); 
  //bubble.render(mouseX,mouseY);  
}
