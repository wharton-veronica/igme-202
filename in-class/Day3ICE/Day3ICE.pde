ArrayList<PVector> points; 

void setup() {
  size(400,400); 
  stroke(255,0,0); 
  strokeWeight(3);
  points = new ArrayList<PVector>(); 
}

void draw() {
  background(50); 
  for (int i = 0; i < points.size()/2; i++) {
    line(points.get(2*i).x, points.get(2*i).y, 
         points.get(2*i+1).x, points.get(2*i+1).y); 
  } 
  if (points.size() % 2 != 0) {
    line(points.get(points.size()-1).x, points.get(points.size()-1).y, mouseX,mouseY);  
  }
  save("image.jpg"); 
}

void mouseClicked() {
  points.add(new PVector(mouseX,mouseY));
   
}
