/** 
 * IGME-202-02
 * Project #1: Braitenberg Vehicles (2D)
 * 
 * @author Veronica Wharton (vlw1338)
 */
 
/**
 * A class the represents a light source in a Braitenberg simulation. 
 * Each light has a position and a brightness that allows a Braitenberg vehicle
 * to determine the influence of a light on its velocity.   
 */
class Light {
  PShape light; 
  PVector pos; 
  int brightness; 
  
  boolean moveWithMouse; 
  
  /**
   * Constructor for a light
   * 
   * @param pos Position of light
   * @param brightness Brightness of light (corresponds to size of light)
   */
  Light(PVector pos, int brightness) {
    this.pos = pos; 
    this.brightness = brightness;
    light = createShape(ELLIPSE, 0, 0, brightness, brightness); 
    light.setFill(color(255,255,0));
    light.setStroke(color(0));
  
    // light is not initially moving with the mouse
    moveWithMouse = false;   
  }
  
  /**
   * Draw light to screen
   */
  void display() {
    
    // if the light is currently being dragged by the mouse, 
    // its center position is the same as the mouse's x- and y-coordinates
    if (moveWithMouse) {
      pos.x = mouseX; 
      pos.y = mouseY;  
    }
    
    // draw the light to the screen
    pushMatrix(); 
    
    translate(pos.x, pos.y); 
    shape(light,0,0); 
    
    popMatrix();  
  }
  
  /**
   * Called by main 'mousePressed' function when mouse button is pressed
   */
  void mousePressed() {
    
    // check for (circle) collision between mouse (just pressed) and each of 
    // the light sources
    if (dist(mouseX, mouseY, pos.x, pos.y) <= brightness) {
      
      // if collision is detected, the light source will follow the mouse
      // until the mouse is released
      moveWithMouse = true;  
    } 
  }
  
  /**
   * Called by main 'mousePressed' function when mouse button is released
   */
  void mouseReleased() {
     moveWithMouse = false; 
  }
}
