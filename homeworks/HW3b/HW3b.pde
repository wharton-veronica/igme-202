Rod rod; 

void setup() {
  size(500,500,P2D); 
  rod = new Rod(new PVector(width/2, height/2)); 
}

void draw() {
  background(50); 
  rod.setEnd(new PVector(mouseX, mouseY)); 
  rod.render(); 
}

void mouseClicked() {
  rod.updateState();  
}


