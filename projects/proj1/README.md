Project #1: Braitenberg Vehicles
-----------------
**Veronica Wharton (IGME-202-02)**

### Overview

This program is based on the [Braitenberg vehicle thought experiment](http://en.wikipedia.org/wiki/Braitenberg_vehicle), in which each vehicle has an artificial intelligence that is based on its environment. Specifically, each vehicle has two sensors that are influenced by the light sources in its environment.  Each sensor is "attached" to one of the car's rear wheels by a wire.  Depending on whether the wires are crossed (i.e.,, right wheel attached to left sensor) or not (i.e., right wheel attached to right sensor), the vehicle is a "coward" (fears the light) or an "aggressor" (is attracted to the light). 

### Design

My program includes two types of vehicles: cowards (red cars) and aggressors (green cars).  Light sources are randomly placed (but evenly distributed) in the environment.  Although each vehicle's movement is impacted by all of the light sources, each vehicle is most impacted by the light sources that are closest to it.

This simulation is intended to run autonomously.  The user cannot directly influence the vehicles' behavior.  Instead, the user can drag light sources from one location to another, but only one light source can be dragged at a time (unless two light sources are overlapping).  A light source will continue to affect the movement of surrounding vehicles even while it is being dragged.  (The user can thus indirectly influence a vehicle's movements by holding a light source near it.)  The user can also show/hide the vehicles' "trails" by pressing SPACE. 

### Implementation

**As part of this simulation, there are `Light` and `Vehicle` classes.**

`Light` objects influence the Braitenberg vehicles in the simulation.  The key components of a `Light` are its position (a `PVector`) and brightness (an `int`).  The starting locations of the lights are random, and the lights are evenly distributed throughout the world.  (Please note that the lights' randomness is implemented in the main `setup` function of this program, so lights' locations could be hard-coded or generated through another random algorithm without needing to change the `Light` class.)  The lights are displayed using circular `PShape`s; the diameter of each circle is the light's brightness.

`Vehicle` objects travel through the environment based on light sources.  During each frame, the following four steps are taken for each vehicle: 

1.  **Determine the absolute position of each of the vehicle's two sensors (`updateSensorPositions()`).**  Although the visualizations of the sensors can be positioned relative to the body of the car using `pushMatrix()`/`popMatrix()`, the actual positions of the sensors must be known absolutely.

2.  **Calculate the car's acceleration from the collective influence of all of the lights on each of the sensors (`getLightInput()`).**  To do this, the distance between each light and each of the car's sensors is calculated.  The effect of a single light on a single sensor is the light's brightness divided by the square of the distance between the light and the sensor.  The cumulative effect of all the lights on a single sensor is the sum of these quotients.  Then, the velocity of each wheel is calculated.  This is the point at which the implementation of a coward differs from than of an aggressor.  For a coward, the right sensor influences the right wheel (and vice versa), while for an aggressor, the right sensor influences the left wheel (and vice versa).  Finally, the acceleration of the entire car is calculated by subtracting the left wheel velocity from the right wheel velocity and rotating the resulting vector by 90 degrees.

3.  **Update the car's velocity and position using the newly-calculated acceleration (`updateVehicle()`).**  Friction is also applied, as the velocity is multiplied by 0.1 after acceleration is added.  Each car also has a terminal velocity of `MAX_SPEED` (currently set to 10).  If a vehicle drives off the edge of the screen, it will reappear at the opposite edge of the screen (`wraparound()`).  

4.  **Display the vehicle to the screen (`display()`).**  (Please note that I attempted to group the `PShape`s within a single `PShape`; however, the resulting `PShape` did not display properly.  I showed this issue to you, and you said it was fine if I instead drew each individual shape to the screen.)

Steps #1-3 are further encapsulated within the `Vehicle`'s `update` method.  

**Additional implementation notes:**

* Since the `Vehicle` class is an inner class within the `proj1` class, vehicles can access all of the `Light`s without having an internal reference to them.  

* A vehicle's angle of rotation was implemented as a `float` rather than as a `PVector` merely as personal preference.  The direction vector mostly exists as a storage container for its angle, so I would prefer to always have access to the angle directly.  

**The two extensions that were implemented are as follows:**

1.  Use vehicles to create attractive drawings
2.  Add user interaction like draggable signal sources

Extension #1 was implemented by adding an `ArrayList` of `PShapes` to the `Vehicle` class.  During each frame, a circular shape that is positioned at the car's current location is added to this list.  The list's contents are displayed during each frame.  The user can toggle the trails' visibility by pressing the spacebar.  (Please note drawing the trails to the screen may slow down the simulation; thus, the user would be prudent to reduce the number of cars in the simulation to 1-3 if the s/he wants to observe the cars' trails for a long period of time.)

Extension #2 was implemented using bounding circle collision detection between the mouse (when pressed) and each of the light sources.  If the mouse is pressed and a collision is detected between it and a light source, the light source will follow the mouse around until the mouse is released.

Overall, I had fun with this project, and I hope you enjoy it!
