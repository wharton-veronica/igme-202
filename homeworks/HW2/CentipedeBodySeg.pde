/**
 * Veronica Wharton
 * IGME-202-02
 * HW #2: Rolling through Random
 */

class CentipedeBodySeg {
  
  PVector pos; 
  float angle;
  final float angleIncrement = PI/6; 
  PShape body, leg, legBehind;  
  
  CentipedeBodySeg(PVector pos, float initialAngle) {
    this.pos = pos; 
    this.angle = initialAngle; 
    
    leg = createShape(RECT, pos.x, pos.y, 5, 20);
    leg.setFill(color(100+random(30)));
    leg.setStroke(false);   
    body = createShape(ELLIPSE, pos.x, pos.y, 30, 40); 
    body.setFill(color(150+random(30)));
    body.setStroke(false);  
    legBehind = createShape(RECT, pos.x, pos.y+10, 5, 20); 
    legBehind.setFill(color(115+random(30)));
    legBehind.setStroke(false);
  } 
  
  void render() {
     shapeMode(CENTER); 
     shape(body, 0, -sin(0.5*angle)*3);
     shape(legBehind, 0, -sin(0.5*angle)*3);
     shape(leg, 0, cos(0.5*angle)*5+18); 

     pos.y += (cos(angle)*5);
     angle += angleIncrement;   
   }
}
