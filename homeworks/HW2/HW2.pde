/**
 * Veronica Wharton
 * IGME-202-02
 * HW #2: Rolling through Random
 */

Grass gr, gr2, gr3;
Centipede centipede, centipede2, centipede3; 

void setup() {
  size(500,300,P2D); 
  
  // create three layers of "grass"
  gr = new Grass(color(#026A22), 0.65*height);
  gr2 = new Grass(100, color(#11BC45), 0.5*height);
  gr3 = new Grass(200, color(#2FA754), 0.3*height);
  
  // create three centipedes
  centipede = new Centipede(new PVector(width*0.75,height*.8));
  centipede2 = new Centipede(new PVector(width*0.5,height*.7));  
  centipede3 = new Centipede(new PVector(width*0.6,height*.55)); 
}

void draw() {
  background(#5DBAFF);
 
  // render from back to front 
  gr3.render();
  centipede3.render();
  gr2.render();
  centipede2.render();  
  gr.render(); 
  centipede.render();
}
