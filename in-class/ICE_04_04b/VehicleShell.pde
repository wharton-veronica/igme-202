/** 
 * IGME-202-02
 * @author Veronica Wharton (vlw1338)
 */

class VehicleShell {
  // moving the vehicle
  PVector pos, veloc, accel;
  float angle; 
  
  // drawing the vehicle 
  PShape sensorL, sensorR, body; 
  float vWidth, vHeight, sideDistance, aheadDistance; 
  float mass; 
  
  // constants
  int DIAM = 10;
  float OFFSET = 1; 
  float MAX_SPEED = 10; 
 
  // sensors
  PVector sensorPosL, sensorPosR; 
  PVector wheelVelocL, wheelVelocR; 
  PVector sensorInputL, sensorInputR; 
  
  /**
   * Constructor for a VehicleShell
   * 
   * @param pos Starting position of vehicle
   * @param mass Mass of vehicle
   * @param sideDistance x-offset for sensor
   * @param aheadDistance y-offset for sensor
   */
  VehicleShell(PVector pos, float mass, float sideDistance, float aheadDistance) {
    this.pos = pos; 
    this.mass = mass; 
    this.sideDistance = sideDistance; 
    this.aheadDistance = aheadDistance; 
     
    vWidth = (sideDistance-OFFSET)*2; 
    vHeight = (aheadDistance-OFFSET)*2; 
    
    sensorPosL = new PVector(-sideDistance, -aheadDistance); 
    sensorPosR = new PVector(sideDistance, -aheadDistance);
  
    createBody();
    
    veloc = new PVector(0, 0); 
    accel = new PVector(0, 0); 
    angle = 0.56; 
  }
  
  /**
   * Initialize and position PShapes that form car's body
   */
  void createBody() {
    body = createShape(RECT,0,0,vWidth,vHeight); 
    body.setFill(color(255,0,0)); 
    
    sensorL = createShape(ELLIPSE,-sideDistance,-aheadDistance,DIAM,DIAM);
    sensorR = createShape(ELLIPSE,sideDistance,-aheadDistance,DIAM,DIAM); 
    sensorL.setFill(color(0,255,255)); // cyan
    sensorR.setFill(color(0,255,255)); // cyan
  }
  
  /**
   * Display car to screen
   */
  void display() {
    pushMatrix(); 
   
    translate(pos.x, pos.y);
    rotate(angle);  
    shapeMode(CENTER); 
    shape(body); 
    shape(sensorL); 
    shape(sensorR); 
    
    popMatrix();  
  }
  
  // must be called after car's position has been updated
  void updateSensorPositions() { 
    
    // distance between center of car and sensor
    float sensorDistance = sqrt(sq(sideDistance) + sq(aheadDistance)); 
    
    // angle of vector between center of car and sensor
    float sensorAngle = atan2(aheadDistance, sideDistance);
   
    // calculate relative positions between sensors and center of car 
    sensorPosR = new PVector(sensorDistance*cos(angle-sensorAngle), 
                             sensorDistance*sin(angle-sensorAngle));
    sensorPosL = new PVector(sensorDistance*cos(angle+PI+sensorAngle), 
                             sensorDistance*sin(angle+PI+sensorAngle));
                             
    // determine absolute positions of sensors
    sensorPosL = PVector.add(sensorPosL, pos); 
    sensorPosR = PVector.add(sensorPosR, pos);
   
    // debugging ellipses
    fill(255,0,0); // red = left sensor
    ellipse(sensorPosL.x, sensorPosL.y, 5, 5); 
    
    fill(0,0,0); // black = right sensor
    ellipse(sensorPosR.x, sensorPosR.y, 5, 5);
    
    fill(255); // white = center of vehicle
    ellipse(pos.x, pos.y, 5, 5);     
  } 
  
  void getLightInput() { 
    sensorInputL = new PVector(0, 0); 
    sensorInputR = new PVector(0, 0);
    
    // iterate through all light sources
    for (Light l : lights) {
      PVector lightDistance = PVector.sub(l.pos, sensorPosL); 
      sensorInputL.add(PVector.mult(lightDistance, l.brightness)); 
      
      lightDistance = PVector.sub(l.pos, sensorPosR);  
      sensorInputR.add(PVector.mult(lightDistance, l.brightness));  
    }
    
    sensorInputL.div(sensorInputR.mag()); 
    sensorInputR.div(sensorInputR.mag()); 
    sensorInputL.mult(50); 
    sensorInputR.mult(50); 
    
    //println(sensorInputL.mag());
    //println(sensorInputR.mag());
    
    // display force on each wheel
    stroke(255,0,0); 
    line(sensorPosL.x, sensorPosL.y, sensorPosL.x+sensorInputL.x, sensorPosL.y+sensorInputL.y);
    stroke(255,0,0); 
    line(sensorPosR.x, sensorPosR.y, sensorPosR.x+sensorInputR.x, sensorPosR.y+sensorInputR.y);
  }
  
  // not needed (done in 'getLightInput' function)
  void calcWheelVelocity() { }
  
  void calculateAcceleration() {
    accel = PVector.add(sensorInputL, sensorInputR);
    accel.div(2); 
    stroke(0,255,0); 
    line(pos.x, pos.y, pos.x+accel.x, pos.y+accel.y); 
  }
  
  void updateVehicle() { 
    veloc.add(accel); 
    veloc.mult(0.2); 
    pos.add(veloc); 
  }
  
}
