/** 
 * IGME-202-02
 * @author Veronica Wharton (vlw1338)
 */
 
class Light {
  PShape light; 
  PVector pos; 
  int brightness; 
  
  /**
   * Constructor for a light
   * 
   * @param pos Position of light
   * @param brightness Brightness of light (corresponds to size of light)
   */
  Light(PVector pos, int brightness) {
    this.pos = pos; 
    this.brightness = brightness;
    light = createShape(ELLIPSE, 0, 0, brightness, brightness); 
    light.setFill(color(255,255,0));
    light.setStroke(false);  
  }
  
  void update() {
    
  }
  
  /**
   * Draw light to screen
   */
  void display() {
    pushMatrix(); 
    
    translate(pos.x, pos.y); 
    shape(light,0,0); 
    
    popMatrix();  
  }
}
