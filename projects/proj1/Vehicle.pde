/** 
 * IGME-202-02
 * Project #1: Braitenberg Vehicles (2D)
 * 
 * @author Veronica Wharton (vlw1338)
 */

/**
 * A class the represents a vehicle in a Braitenberg simulation. 
 * Depending on the value of the 'coward' boolean in the constructor, 
 * the vehicle will either be attracted to or scared of light sources. 
 */
class Vehicle {
  /* MOVING THE VEHICLE */
  // movement
  PVector pos, veloc, accel;
  float angle;
 
  boolean coward = true;  
  
  // constants
  float MAX_SPEED = 10; 
  
  // sensors
  PVector sensorPosL, sensorPosR;
  
  // sensor inputs from lights
  float sensorInputL, sensorInputR;
  
  /* DRAWING THE VEHICLE */
  PShape sensorL, sensorR, body; 
  float vWidth, vHeight, sideDistance, aheadDistance; 
  float mass; 
  
  // constants
  int DIAM = 8;        
  float OFFSET = 1;      
 
  // list of ellipses that show where the car has been
  ArrayList<PShape> trail = new ArrayList<PShape>(); 
  boolean showTrail;  
   
  /**
   * Constructor for a Vehicle
   * 
   * @param pos Starting position of vehicle
   * @param mass Mass of vehicle
   * @param sideDistance x-offset for sensor
   * @param aheadDistance y-offset for sensor
   * @param coward Whether or not the vehicle is scared of the light sources
   */
  Vehicle(PVector pos, float mass, float sideDistance, 
    float aheadDistance, boolean coward) {
      
    // initialize internal state
    this.pos = pos; 
    this.mass = mass; 
    this.sideDistance = sideDistance; 
    this.aheadDistance = aheadDistance; 
    this.coward = coward; 
     
    // width and height (aka length) of car
    vWidth = (sideDistance-OFFSET)*2; 
    vHeight = (aheadDistance-OFFSET)*2; 
    
    // starting sensor positions
    sensorPosL = new PVector(-sideDistance, -aheadDistance); 
    sensorPosR = new PVector(sideDistance, -aheadDistance);
  
    // create visualization of car
    createBody();
    
    // initialize velocity, acceleration, angle
    veloc = new PVector(0, 0); 
    accel = new PVector(0, 0); 
    angle = 0; 
    
    // trail is initially showing
    showTrail = true; 
  }
  
  /**
   * Initialize and position PShapes that form car's body
   */
  void createBody() {
    body = createShape(RECT,0,0,vWidth,vHeight); 
    if (coward) {
      body.setFill(color(255,0,0));
    } 
    else {
      body.setFill(color(0,255,0));
    }
    
    sensorL = createShape(ELLIPSE,-sideDistance,-aheadDistance,DIAM,DIAM);
    sensorR = createShape(ELLIPSE,sideDistance,-aheadDistance,DIAM,DIAM); 
    sensorL.setFill(color(0,255,255)); // cyan
    sensorR.setFill(color(0,255,255)); // cyan
  }
  
  /**
   * Display car to screen
   */
  void display() {
    
    // draw the car's path to the screen
    // slows down the simulation significantly, so reducing the number
    // of cars is a good idea
    if (showTrail) {
      /*if (trail.size() > 300) {
        trail.remove(0);  
      }*/
      for (int i = 0; i < trail.size(); i++) {
        shape(trail.get(i)); 
      }
    }
    
    // draw car to screen
    pushMatrix(); 
   
    translate(pos.x, pos.y);
    rotate(angle);  
    shapeMode(CENTER); 
    
    // I attempted to group the PShapes; however, the resulting PShape did
    // not display properly.  I showed this issue to you, and you said it was
    // fine if I instead drew each individual shape to the screen
    shape(body); 
    shape(sensorL); 
    shape(sensorR); 
    
    popMatrix();
  }
  
  /**
   * Update the car's internal state
   */
  void update() {
    updateSensorPositions(); 
    getLightInput(); 
    updateVehicle();  
  }
  
  /**
   * Update the absolute (NOT relative) locations of each 
   * of the car's sensors
   */
  void updateSensorPositions() { 
    
    // distance between center of car and sensor
    float sensorDistance = sqrt(sq(sideDistance) + sq(aheadDistance)); 
    
    // angle of vector between center of car and sensor
    float sensorAngle = atan2(aheadDistance, sideDistance);
   
    // calculate relative positions between sensors and center of car 
    sensorPosR = new PVector(sensorDistance*cos(angle-sensorAngle), 
                             sensorDistance*sin(angle-sensorAngle));
    sensorPosL = new PVector(sensorDistance*cos(angle+PI+sensorAngle), 
                             sensorDistance*sin(angle+PI+sensorAngle));
                             
    // determine absolute positions of sensors
    sensorPosL = PVector.add(sensorPosL, pos); 
    sensorPosR = PVector.add(sensorPosR, pos);  
  } 
  
  /**
   * Determine the influence of the light sources on the car's
   * acceleration.  Update the car's acceleration.
   */
  void getLightInput() { 
    sensorInputL = 0; 
    sensorInputR = 0;
    
    // iterate through all light sources
    for (Light l : lights) {
      
      // get distance between left sensor and light
      PVector lightDistance = PVector.sub(l.pos, sensorPosL);
      
      // determine light's influence on left sensor
      float influence = l.brightness / lightDistance.magSq(); 
      sensorInputL += influence; 
     
      // get distance between right sensor and light
      lightDistance = PVector.sub(l.pos, sensorPosR);
      
      // determine light's influence on right sensor
      influence = l.brightness / lightDistance.magSq(); 
      sensorInputR += influence; 
    }  
    
    sensorInputL *= 10; 
    sensorInputR *= 10; 
        
    // vector between center of car and sensor
    // different behavior depending on whether vehicle is cowardly
    // or aggressive
    PVector endpointL, endpointR; 
    if (coward) {
      // left wheel velocity
      endpointL = new PVector(sensorInputL*cos(angle-PI/2), 
                              sensorInputL*sin(angle-PI/2));
      // right wheel velocity
      endpointR = new PVector(sensorInputR*cos(angle-PI/2), 
                              sensorInputR*sin(angle-PI/2));
    }
    else {
      // left wheel velocity
      endpointL = new PVector(sensorInputR*cos(angle-PI/2), 
                              sensorInputR*sin(angle-PI/2));
      // right wheel velocity
      endpointR = new PVector(sensorInputL*cos(angle-PI/2), 
                              sensorInputL*sin(angle-PI/2));
    }
    
    // draw velocity vectors for each sensor
    /*stroke(255,0,0); 
    line(sensorPosL.x, sensorPosL.y, 
      sensorPosL.x+endpointL.x, sensorPosL.y+endpointL.y);
    stroke(255,0,0); 
    line(sensorPosR.x, sensorPosR.y, 
      sensorPosR.x+endpointR.x, sensorPosR.y+endpointR.y);*/
    
    // calculate acceleration
    PVector diff = PVector.sub(
        PVector.add(sensorPosR, endpointR), 
        PVector.add(sensorPosL, endpointL)); 
    diff.rotate(-PI/2); // left perpendicular
    accel = diff; 
   
    // draw heading vector
    /*stroke(255,255,0); 
    line(pos.x, pos.y, pos.x+accel.x, pos.y+accel.y);*/
    
  }
  
  /**
   * After the acceleration of the vehicle has been calculated, 
   * update the vehicle's velocity, position, and angle. 
   * Also, add to the Vehicle's "trail" -- a list of ellipses
   * that show where the car has traveled during the simulation.
   */
  void updateVehicle() { 
    
    veloc.add(accel);
    veloc.mult(0.1);  // friction
    veloc.limit(MAX_SPEED); 
    pos.add(veloc);

    angle = accel.heading()+PI/2; 
    
    wraparound(); 
   
    PShape circle = createShape(ELLIPSE, pos.x, pos.y, 3, 3);
    circle.setStroke(false);  
    if (coward) {
      circle.setFill(color(170, 5, 38)); // dark red
    } 
    else {
      circle.setFill(color(84, 170, 5)); // dark green
    }
    trail.add(circle); 
    
  }
  
  /**
   * Ensure that if the vehicle drives off the screen, it will reappear 
   * elsewhere on the screen instead of disappearing forever.
   */
  void wraparound() {
    int WRAPAROUND = 20; 
    if (pos.x < -WRAPAROUND) {
      pos.x = width+WRAPAROUND; 
      
      // car slows down whenever it wraps around
      veloc.mult(0.01); 
    } 
    else if (pos.x > width+WRAPAROUND) {
      pos.x = -WRAPAROUND;
      
      // car slows down whenever it wraps around
      veloc.mult(0.01);  
    }
    if (pos.y < -WRAPAROUND) {
      pos.y = height + WRAPAROUND; 
      
      // car slows down whenever it wraps around
      veloc.mult(0.01);  
    }
    else if (pos.y > height+WRAPAROUND) {
      pos.y = -WRAPAROUND; 
      
      // car slows down whenever it wraps around
      veloc.mult(0.01); 
    }
  }
  
}
