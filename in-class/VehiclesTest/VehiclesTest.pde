Vehicle v; 

void setup() {
  size(500,500,P2D); 
  v = new Vehicle(new PVector(width/2,height/2)); 
}

void draw() {
  v.render(); 
}
