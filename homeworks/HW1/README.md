HW #1: Bouncing Balls
---------------------

**Veronica Wharton (IGME-202-02)**

My app allows users to create bouncing balls.  The user can adjust the size
of each ball by pressing and holding the mouse button.  While the mouse is
pressed, the ball grows and shrinks; when the mouse is released, the diameter 
of the ball is saved, and the ball is released and begins to bounce. 

The balls accurately collide with the bottom of the frame, but they do not 
react to other balls.  In short, each bouncing ball exists in its own reality.

To export a frame as an image, the user can press the spacebar.  More than 
one frame can be saved, and all will be stored in the 'HW1' folder.

My drawing package is cool because it involves physics and thus is believable.
It's also cool because the bouncing balls' saturated colors coupled with the
chalkboard-esque background is aesthetically appealing.
