HW#2: Rolling through Random
----------------------------

**Veronica Wharton (IGME-202-02)**

My project includes three centipedes that appear to be traveling
through grass.

The grass is created using Perlin Noise values.  The index that is
used to generate the Perlin Noise values is incremented by a large
amount (0.7), which accounts for the jagged, grass-like appearance of
the shape.  

The movement of the centipede body segments and legs is dictated by a
sinusoid.  Specifically, the leg's movement is simulated using cos(x),
and the body's movement is simulated using -sin(x).

All of the centipede body segements are grouped into a 'Centipede'
class, which has a 'render' method that then calls the 'render
method  of each of its body segments.  The main program thus only 
has to 'render' the entire centipede.

I chose to make centipedes because of the challenge of animating their
body parts in a realistic way.  I think that the resulting wave effect
is pretty awesome.
