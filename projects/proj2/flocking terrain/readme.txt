Project #2: Ships in Outer Space
Veronica Wharton
IGME-202-02

The world in my project is outer space-themed.  In it, a fleet of four space
ships are endlessly traveling from planet to planet in a solar system.

My project implements the following steering behaviors: flocking, seeking, and 
arriving.  All of these behaviors are exhibited by the space ships.  At all 
times, the space ships are seeking a specific planet.  When they are within a 
certain radius of their target planet, they begin to slow down (arrival).  When 
they are within a yet smaller radius of their target planet, they have 
"arrived," and they are assigned a new target planet.  (The target planet is a 
global variable, so once a single ship has arrived at the target, all of the 
ships begin to seek a new target.)  The ships also avoid obstacles -- they avoid 
the sun and all of the planets (excluding their current target.)

The planets' sizes and locations in the world are randomly generated.  
The planets are stored in a global array that all ships can access.
The sun is also globally accessible.  The target planet is updated simply by 
moving to the next planet in the planets array; when the end of the array is 
reached, the global index is reset to zero, and the first planet in the array is
sought again. 

The space ship model was imported from the Unity Asset Store.  The sun and 
planets are primitive spheres with added textures.  The stars in the background 
are actually a suspended particle system, and are thus also Unity primitives.