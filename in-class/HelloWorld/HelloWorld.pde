size(500,500);
/*background(100,149,237); 
stroke(124,252,0); 
line(10,10,60,10); 
line(10,10,10,60); 
line(10,60,60,60);
line(60,60,60,10); 
println("Hello, World!");*/ 


background(#003FDB); 
strokeWeight(4); 

// lines of random length that span the width
for (int i = 0; i < 100; i++) {
  
  // lines from bottom to top
  int h = int(random(0,height)); 
  stroke(124,252,0); // green
  line(width/100*i,height,width/100*i,height-h);
  
  // lines from top to bottom
  h = int(random(0,height)); 
  stroke(252,124,0);   
  line(width/100*i,0,width/100*i,h); 
}

// semi-transparent ellipses of random and location
fill(50,50);
noStroke(); 
for (int i = 0; i < 20; i++) {
  ellipse(random(width),random(height),random(25,75),random(25,75));  
}

println("Some clever message");
