class Bubble {
  PShape bubble;
  PVector pos; 
  int t = 0;
  int factor1, factor2;   
  
  Bubble(PVector pos) {
    this.pos = pos; 
    bubble = createShape(GROUP);
    fill(#BFF6FF); // light blue
    PShape outside = createShape(ELLIPSE, 0, 0, 30, 30); 
    noStroke();  
    fill(#E0FBFF); 
    PShape middle = createShape(ELLIPSE, 9, 7, 15, 15); 
    fill(#ffffff); 
    PShape inside = createShape(ELLIPSE, 18, 7, 8, 8); 
    bubble.addChild(outside);
    bubble.addChild(middle); 
    bubble.addChild(inside); 
    factor1 = int(random(6,15)); 
    factor2 = int(random(6,15));
  } 
  
  void render(int x, int y) {
    shape(bubble, x, y);  
  }
  
  void render() {
    t += 1; 
    pos.x += cos(t/factor1); 
    pos.y += sin(t/factor2); 
    render(int(pos.x), int(pos.y)); 
  }
}
