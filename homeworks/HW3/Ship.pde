/**
 * HW #3: Asteroids
 * IGME-202-02
 * 
 * @author Veronica Wharton (vlw1338)
 */

/**
 * A class that represents a user-controlled spaceship
 * in the game 'Asteroids.' 
 */
public class Ship {
  // things that change
  PVector pos;           // ship's current angle of rotation
  float angle = 0;       // ship's current location
  float speed = 0;       // ship's current speed  
  
  // things that don't change
  PShape ship;           // visualization of ship  
  int size = 40;         // scaling factor for ship image
  float deltaAngle = PI/50; // "speed" of ship rotation
  PShape flame;          // visualization of ship's speed
  int maxSpeed = 10;     // ship's terminal velocity
  
  // array of bullets
  ArrayList<Bullet> bullets = new ArrayList<Bullet>(); 
 
  // boolean flags to record key(s) currently pressed
  boolean rightPressed = false, 
          leftPressed = false, 
          upPressed = false, 
          downPressed = false;  

  /**
   * Constructor for a ship
   * 
   * @param pos Ship's starting position
   */
  public Ship(PVector pos) {
    this.pos = pos; 
    
    // create a triangle to represent the ship
    ship = createShape(); 
    ship.beginShape(); 
    ship.vertex(size/2,0);            // top corner
    ship.vertex(-size/2,size/3);      // right corner
    ship.vertex(-size/2,-size/3);     // left corner
    ship.vertex(size/2,0);
    ship.endShape(); 
    ship.setFill(false); 
    ship.setStroke(color(255));
   
    // create a fuel flame for the ship 
    flame = createShape(); 
    flame.beginShape(); 
    flame.vertex(-0.6*size, 0.1*size); 
    flame.vertex(-0.7*size, 0.2*size); 
    flame.vertex(-size, 0); 
    flame.vertex(-0.7*size, -0.2*size);
    flame.vertex(-0.6*size, -0.1*size);   
    flame.endShape(); 
    flame.setFill(false); 
    flame.setStroke(color(255)); 
  } 
  
  /**
   * Draw the ship to the screen. 
   * Also handles location update based on keys pressed.
   */
  public void render() {
     // rotate CW
     if (rightPressed) { angle += deltaAngle; }
     
     // rotate CCW
     if (leftPressed) { angle -= deltaAngle; }
     
     // angle wraparound
     angle %= (2*PI);
    
     // acceleration
     if (upPressed) {
       speed += 0.2; 
     }
     
     // friction
     speed *= 0.98;
      
     if (speed > maxSpeed) speed = maxSpeed; 
     
     // update position
     pos.x += (speed * cos(angle)); 
     pos.y += (speed * sin(angle));
     wraparound(pos, size);  
     
     // draw bullets to screen
     drawBullets(); 
     
     // draw ship to screen
     pushMatrix(); 
     translate(pos.x, pos.y); 
     rotate(angle); 
     shape(ship, 0, 0);
     
     // only show flame if ship is accelerating
     if (upPressed) {
       if (random(1) < 0.5) {
         scale(1.1); 
       }
       shape(flame, 0, 0);
     }
     popMatrix(); 
  }
  
  /**
   * Draw bullet's to the screen
   * If a bullet has been alive for more than 40 frames, 
   * it is removed.
   */
  private void drawBullets() {
    Iterator<Bullet> i = bullets.iterator(); 
     while (i.hasNext()) {
       Bullet b = i.next(); 
       if (b.age() > 40) {
         i.remove(); 
       } 
       else {
         b.render();  
       }
     }
  }
  
  /**
   * Controls ship's reactions to keys being pressed.
   * Specifically, toggles to true the boolean flags associated with
   * the arrow keys and spacebar.
   * 
   * RIGHT and LEFT arrow keys control the ship's angle 
   * of rotation. UP arrow key controls the ship's velocity.
   * SPACEBAR results in the ship firing a bullet.
   * 
   * @param key_ Equivalent to 'key' variable that is set when a 
   * key is pressed
   * @param keyCode_ Equivalent to 'keyPresed' variable that is 
   * set when a key is pressed
   */
  public void shipKeyPressed(int key_, int keyCode_) {
    if (keyCode_ == RIGHT) {
      rightPressed = true;  
    }     
    if (keyCode_ == LEFT) {
      leftPressed = true;  
    } 
    if (keyCode_ == UP) {
      upPressed = true;  
    }
    
    // if space bar is pressed, add a new bullet
    if (key_ == ' ') {
      bullets.add(new Bullet(
        new PVector(pos.x, pos.y), 
        angle, 
        10
      ));  
    }
  }
  
  /**
   * Toggles to false the boolean flags associated with
   * the arrow keys and spacebar.
   * 
   * @param key_ Equivalent to 'key' variable that is set when a 
   * key is pressed
   * @param keyCode_ Equivalent to 'keyPresed' variable that is 
   * set when a key is pressed
   */
  public void shipKeyReleased(int key_, int keyCode_) {
    if (keyCode_ == RIGHT) {
      rightPressed = false;  
    }     
    if (keyCode_ == LEFT) {
      leftPressed = false;  
    } 
    if (keyCode_ == UP) {
      upPressed = false;  
    }
  }
  
  /**
   * Getter for a Iterator for 'bullets'
   * 
   * @return an Iterator for 'bullets'
   */
  public Iterator<Bullet> bulletIterator() {
    return bullets.iterator();  
  }
  
  /** 
   * Getter for ship's size (scaling factor)
   * 
   * @return ship's size
   */
  public int size() {
    return size;  
  }
  
  /** 
   * Getter for ship's current position
   * 
   * @return ship's current position
   */
  public PVector pos() {
    return pos; 
  }
}
