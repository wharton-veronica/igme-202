/**
 * Veronica Wharton
 * IGME-202-02
 * HW #2: Rolling through Random
 */

class Centipede {
  
  CentipedeBodySeg[] body; 
  
  float angle = 0;
  final float angleIncrement = PI/24;  
  
  Centipede(PVector start) {
    body = new CentipedeBodySeg[20]; 
 
    for (int i = 0; i < body.length; i++) {
      body[i] = new CentipedeBodySeg(start,angle); 
      start.x -= 27; 
      angle -= PI/2; 
    }
  } 
  
  void render() {
     for (int i = 0; i < body.length; i++) {
       body[i].render();  
     }
  }
}
