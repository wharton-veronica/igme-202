/**
 * Veronica Wharton
 * IGME-202-02
 * HW #1: Drawing Application
 */

// store all bouncing balls
ArrayList<Ball> balls;

// variables for new ball creation
float newBallDiam = 5.0;
float minBallDiam = 5.0, maxBallDiam = 50.0; 
boolean ballIncreasing = true; 
color newBallCol; 

void setup() {
  size(200,600); 
  balls = new ArrayList<Ball>(); 
}

void draw() {
  background(50); // dark gray
  
  // draw all bouncing balls
  for (int i = 0; i < balls.size(); i++) {
      balls.get(i).render();
  }
  
  // interface for new ball creation
  // ball grows and shrinks while the mouse is pressed
  //   so that the user can pick the size of the boucing ball
  if (mousePressed) {
    if (ballIncreasing) {
      newBallDiam += 1; 
    }
    else {
      newBallDiam -= 1;  
    }
    
    if (newBallDiam >= maxBallDiam) {
       ballIncreasing = false;
       newBallDiam = maxBallDiam; 
    }
    else if (newBallDiam <= minBallDiam) {
       ballIncreasing = true;  
       newBallDiam = minBallDiam;
    }
    
    fill(newBallCol); 
    ellipse(mouseX, mouseY, newBallDiam, newBallDiam);
  }
}

/**
 * When mouse is pressed, the color of the next ball is generated
 * and saved
 */
void mousePressed() {
   newBallCol = color(random(-100,500),
                      random(-100,500),
                      random(-100,500)); 
}

/** 
 * When mouse is released, a new Ball is created and stored
 */
void mouseReleased() {
  balls.add(new Ball(newBallDiam, new PVector(mouseX,mouseY), newBallCol));
  
  // reset to default ball diameter
  newBallDiam = 5.0;
}

/**
 * When SPACEBAR is pressed, an image of the current frame is saved
 */
void keyPressed() {
  if (key == ' ') {
    saveFrame(); 
  } 
}


