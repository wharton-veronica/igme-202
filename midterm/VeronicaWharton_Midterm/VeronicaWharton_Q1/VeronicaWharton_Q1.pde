/**
 * Midterm Practical Exam: Question #1
 * IGME-202-02
 * Veronica Wharton
 */

int[] randomNumbersArray;   // array to hold random numbers 
int[] graphHeightArray;     // array to hold frequencies of random numbers

void setup() {
  size(500,500); 
  setupRandom();
}

void draw() {
  background(255); 
  drawBarGraph(); 
}

/**
 * Generate values for bar graph
 */
void setupRandom() {
  randomNumbersArray = new int[100];
  graphHeightArray = new int[10];
  
  // generate 100 random numbers between 0 and 9
  // record the frequency of each number for later visualization
  for (int i = 0; i < randomNumbersArray.length; i++) {
    int num = int(random(0,10));
    randomNumbersArray[i] = num;
    graphHeightArray[num]++; 
  }
}

/**
 * Draw bar graph to screen
 */
void drawBarGraph() {
  int numBars = graphHeightArray.length; 
  int barWidth = width/numBars; 
  
  strokeWeight(3); 
  rectMode(CORNERS); 
  
  // iterate through graphHeighArray
  for (int i = 0; i < graphHeightArray.length; i++) {
    
    // increasing transparency from L to R
    fill(255,0,0,255/numBars*i); 
    
    // draw bar
    int barHeight = graphHeightArray[i]*20; 
    rect(i*barWidth, height, 
         i*barWidth+barWidth, height-barHeight);
  } 
}

/**
 * If space bar is pressed, regenerate the random numbers and their frequencies
 */
void keyPressed() {
  if (key == ' ') {
    setupRandom(); 
  } 
}
