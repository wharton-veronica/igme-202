// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

//Used for IGME 202

//@author Veronica Wharton (vlw1338)


class Mover {

  //Class-wide Variables -------------------------------------------------
  PVector location;
  PVector velocity;
  PVector acceleration;
  PVector force;
  PVector friction;
  float mass;
  int radius;
  float mu = 0.01;      // coefficient of friection
  color c; 

  //Mover constructor ----------------------------------------------------
  Mover(float m_, int radius_) {
    location = new PVector(30,30);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,2);
    force = new PVector(0,0);
    friction = new PVector(0,0);
    mass = m_;
    radius = radius_;
    c = color(int(random(-100,500)), int(random(-100,500)), int(random(-100,500))); 
  }

  //update function ------------------------------------------------------
  //updates this Mover's acceleration and velocity to calculate the Mover's next position
  void update() {
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
  }
  
  //APPLYFORCE FUNCTION --------------------------------------------------
  //IMPLEMENT THE APPLYFORCE FUNCTION BELOW
  void applyForce(PVector force_){
    //CALCULATE THE FORCE VECTOR BASED UPON THE CONCEPT OF MASS
    //USE THE CLASS'S FORCE VECTOR TO STORE THIS CALCULATED FORCE
    //F = MA, SO A = F/M
    //THEN ADD THAT FORCE VECTOR TO OUR ACCELERATION VECTOR
    
    PVector newAccel = PVector.div(force_, mass);
    acceleration.add(newAccel);
  }
  
  
  //CALCULATE FRICTION FUNCTION -----------------------------------------
  //WRITE A FUNCTION THAT WILL CORRECTLY CALCULATE THE FORCE OF FRICTION
  //BASED UPON THIS OBJECT'S MASS
  //SEE DANIEL SHIFFMAN'S CODE IN EXAMPLE 2.4 http://natureofcode.com/book/chapter-2-forces/
  void calculateFriction(){
    //GRAB THIS OBJECT'S VELOCITY AND REVERSE ITS X AND Y VALUES
    //NORMALIZE AND MULTIPLY BY THE COEFFICIENT OF FRICTION
    //ADD TO ACCELERATION VECTOR
    
    friction = velocity.get();
    friction.normalize();
    friction.mult(-1);
    friction.mult(mu);
  }


  //display function ----------------------------------------------------
  //draws the Mover to the sketch window
  void display() {
    stroke(0);
    strokeWeight(2);
    fill(c);
    ellipse(location.x,location.y, radius, radius);
  }


  //checkEdges function -------------------------------------------------
  //Keeps this Mover within the bounds of the window
  //If Mover approaches the edges, it multiplies its velocity by -1 to reverse direction
  void checkEdges() {
    if (location.x > width) {
      location.x = width;
      velocity.x *= -1;
    } 
    else if (location.x < 0) {
      velocity.x *= -1;
      location.x = 0;
    }
    if (location.y > height) {
      velocity.y *= -1;
      location.y = height;
    }
    else if (location.y < 0){
      velocity.y *= -1;
      location.y = 0; 
    }
  }
}


