
//Version 5:
//Includes wrapping AND up/down movement

Mover mover;
boolean isMovingRight = false;
boolean isMovingLeft = false;
boolean isMovingUp = false;
boolean isMovingDown = false;
 
void setup() {
  size(800,800,P2D);
  mover = new Mover(5);
}
 
void draw() {
  background(255);
  mover.updatePosition();
  mover.display();
}

//move the shape left or right when the left and right arrow keys are pressed
void keyPressed() {
  if(keyCode == RIGHT) {
      isMovingRight = true;
  } 
  if(keyCode == LEFT) {
      isMovingLeft = true;
  } 
  if(keyCode == UP) {
      isMovingUp = true;
  } 
  if(keyCode == DOWN) {
      isMovingDown = true;
  }
}

//the difference between using this and setting the isMoving bools to false 
//  in the draw function is that this allows smooth movement while the 
//  key is down.
void keyReleased() {
  if(keyCode == RIGHT) {
      isMovingRight = false;
  } 
  if(keyCode == LEFT) {
      isMovingLeft = false;
  } 
  if(keyCode == UP) {
      isMovingUp = false;
  } 
  if(keyCode == DOWN) {
      isMovingDown = false;
  }
} 
