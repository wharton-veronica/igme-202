/**
 * Midterm Practical Exam: Question #2
 * IGME-202-02
 * Veronica Wharton
 */

MovingShape movingShape; 
boolean turning;         // whether or not the shape should rotate 20 degrees CW

void setup() {
  size(500,500,P2D); 
  movingShape = new MovingShape(1); 
}

void draw() {
  // redraw background
  background(50); 
  
  // update shape's position
  movingShape.update(); 
  
  // display shape to screen
  movingShape.display(); 
  
  // reset boolean at the end of each frame
  turning = false; 
}

/**
 * If left mouse button is pressed, set 'turning' to true
 */ 
void mouseClicked() {
  if (mouseButton == LEFT) {
      turning = true; 
  }
}
