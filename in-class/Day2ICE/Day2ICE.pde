void setup() {
  size(400,250); 
  smooth();  
}

void draw() {
  background(255); 
  
  // generate grayscale bar
  // this creates a cohesive-looking "bar"
  // rectangles are 2 pixels x 50 pixels with ascending alpha levels
  noStroke(); 
  for (int i = 0; i < 255; i++) {
    fill(i); 
    rect(i*width/255,30,2,50); 
  } 
    
  // cyan bar
  fill(0,255,255); 
  rect(0,120,width,30); 
  
  // magenta bar
  fill(255,0,255);
  rect(0,150,width,30); 
  
  // yellow bar
  fill(255,255,0);
  rect(0,180,width,30); 
  
  // black bar
  fill(0); 
  rect(0,210,width,30);
 
 // user interaction
 rect(mouseX,mouseY,50,50);  
}
