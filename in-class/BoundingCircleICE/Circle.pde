/** 
 * A class that represents a circle.
 * 
 * @author Veronica Wharton (vlw1338)
 */
class Circle {
  
  PVector pos; 
  int rad; 
  color c, defaultC; 
  PShape circle; 
  
  /** 
   * Constructor for a Circle
   * @param rad The circle's radius
   * @param c The circle's (default) fill color 
   */
  Circle(int rad, color c) {
    this.rad = rad; 
    this.defaultC = c; 
    this.c = c; 
    circle = createShape(ELLIPSE, 0, 0, rad*2, rad*2);
  }
  
  /**
   * Getter for circle's radius
   * @return The circle's radius
   */
  int radius() {
    return rad; 
  } 
  
  /**
   * Getter for circle's current position
   * @return A vector representing the coordinates of the 
   * center of the circle
   */
  PVector center() {
    return pos; 
  }
  
  /** 
   * Setter for the circle's current position
   * @param theNewCenter Coordinates for the circle's new center
   */
  void setCenter(PVector theNewCenter) {
    pos = theNewCenter;  
  }
  
  /**
   * Draw the circle to the screen
   */
  void render() { 
    // render settings
    shapeMode(CENTER); 
    circle.setFill(c); 
    circle.setStroke(false);
   
    // draw circle to screen 
    shape(circle, pos.x, pos.y); 
  }
  
  /** 
   * Setter for circle's fill color
   * @param theNewC The new fill color for the circle
   */
  void setFill(color theNewC) {
    c = theNewC; 
  }
  
  /** 
   * Restore circle's fill color to the original 
   * (as given in the constructor)
   */
  void setDefaultFill() {
    c = defaultC; 
  }
}
