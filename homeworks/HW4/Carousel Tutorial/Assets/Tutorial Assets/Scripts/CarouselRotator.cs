﻿/**
 * Veronica Wharton
 * IGME-202-02
 * HW #4: The Carousel (in Unity)
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarouselRotator : MonoBehaviour {

	private Vector3 rotationAxis = Vector3.up; 
	private float rotationSpeed = 60.0f;

	public GameObject BlueRocketPrefab; 
	public GameObject RedRocketPrefab; 

	// list of all rockets
	List<GameObject> rockets;

	// list of all initial y-coordinates (for rockets' movements)
	List<float> initialYs; 

	// used to move the rockets up and down
	private float frequency = 4.0f; 
	private float magnitude = 0.01f; 

	// Use this for initialization
	void Start () {

		// initialize lists
		rockets = new List<GameObject>(); 
		initialYs = new List<float>(); 

		int NUM_ROCKETS = 8; 
		for (int i = 0; i < NUM_ROCKETS; i++) {

			// create a new blue rocket and add to list
			if ( i%2 == 0 )  {
				rockets.Add( (GameObject) Instantiate( BlueRocketPrefab, new Vector3( 0, 0, 0 ), Quaternion.identity ) ); 
			}

			// create a new red rocket and add to list
			else {
				rockets.Add( (GameObject) Instantiate( RedRocketPrefab, new Vector3( 0, 0, 0 ), Quaternion.identity ) );
			}
		}

		// set the initial positions, rotations, scales of all rockets
		float angle = 0; 
		float SCALE = 0.65f; 
		float OFFSET = 0.09f;
		int j = 0; 
		foreach (GameObject rocket in rockets ) {

			// assign the carousel as the rocket's parent
			rocket.transform.parent = transform; 
			
			// scale the rocket in all dimensions
			rocket.transform.localScale += new Vector3( SCALE, SCALE, SCALE ); 
			
			// position the rocket at the same location as its parent
			rocket.transform.localPosition = transform.localPosition;
			rocket.transform.localPosition -= new Vector3(0, 0.165f, 0); 

			float xOffset = OFFSET * Mathf.Cos(angle); 
			float zOffset = OFFSET * Mathf.Sin(angle); 
			rocket.transform.localPosition += new Vector3( xOffset, 0, zOffset );

			float rotAngle = Mathf.Rad2Deg * -angle; 
			rocket.transform.Rotate( Vector3.up, rotAngle );

			// red rockets require an additional rotation
			if ( j%2 != 0 ) {
				rocket.transform.Rotate( Vector3.right, -90 ); 
			}

			j++; 
			angle += (2*Mathf.PI)/NUM_ROCKETS; 

			initialYs.Add(rocket.transform.localPosition.y); 
		}
	}
	
	// Update is called once per frame
	void Update () {

		// rotate the carousel
		transform.Rotate( rotationAxis, rotationSpeed * Time.deltaTime ); 

		// rocket oscillations
		int i = 0; 
		foreach (GameObject rocket in rockets ) {

			float nextY; 

			// blue rockets
			if ( i%2 == 0 ) {
				nextY = initialYs[i] + magnitude * Mathf.Cos( frequency * Time.time ); 
			}

			// red rockets
			else {
				nextY = initialYs[i] + magnitude * -Mathf.Cos( frequency * Time.time );
			}

			rocket.transform.localPosition = new Vector3( rocket.transform.localPosition.x, nextY, rocket.transform.localPosition.z );

			i++; 
		}
	}
}
