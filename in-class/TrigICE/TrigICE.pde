PShape octagon; 

void setup() {
  size(500,500,P2D);
  background(255,0,0);
 
  int centerX = width/2, centerY = height/2; 
  octagon = createShape(); 
  octagon.beginShape(); 
  
  // vertices based on trig
  int r = 30; 
  for (float i = 0; i < 2*PI; i += PI/4) {
    octagon.vertex(centerX+r*cos(i),centerY+r*sin(i));  
  }
  
  // hard-coded vertices
  /*octagon.vertex(centerX+10,centerY+0); 
  octagon.vertex(centerX+7,centerY+7); 
  octagon.vertex(centerX+0,centerY+10);
  octagon.vertex(centerX-7,centerY+7);
  octagon.vertex(centerX-10,centerY+0);
  octagon.vertex(centerX-7,centerY-7);
  octagon.vertex(centerX+0,centerY-10); 
  octagon.vertex(centerX+7,centerY-7);*/
 
  octagon.endShape(CLOSE);   
}

void draw() {
  shape(octagon);  
}
