/**
 * HW #3: Asteroids
 * IGME-202-02
 * 
 * @author Veronica Wharton (vlw1338)
 */

/** 
 * A class that represents a moving asteroid.
 */
public class Asteroid {
  PVector pos;       // asteroid's current position
  PVector velocity;  // asteroid's velocity (randomly generated) 
                     // (constant throughout lifetime)
  int state = 0; 
  
  PShape asteroid;   // visualization of asteroid
  int diameter;      // asteroid's diameter (randomly generated)
   
  // constants
  int ALIVE = 0, DEAD = 1;
  float MAX_SPEED = 1.2; 
 
  /**
   * Constructor for an asteroid.
   * Position, size, and velocity are randomly generated.
   */
  public Asteroid() {
    diameter = int(random(10,40)); 
    asteroid = createShape(ELLIPSE, 0, 0, diameter, diameter);
    asteroid.setFill(false); 
    asteroid.setStroke(color(255));
    velocity = new PVector(random(-MAX_SPEED, MAX_SPEED), random(-MAX_SPEED, MAX_SPEED)); 
    
    float rand = random(1); 
    
    // left side of screen
    if (rand < 0.25) {
      pos = new PVector(0, random(0, height));
    }
    
    // right side of screen
    else if (rand < 0.5) {
      pos = new PVector(width, random(0, height));
    }
    
    // top side of screen
    else if (rand < 0.75) {
      pos = new PVector(random(0, width), 0);
    }
    
    // bottom side of screen
    else {
      pos = new PVector(random(0, width), height);
    }
       
  } 
  
  /**
   * Update the asteroid's position.
   */
  private void move() {
      pos.add(velocity); 
  }
  
  /**
   * Update the asteroid's position, and 
   * draw the asteroid to the screen.
   */ 
  public void render() {
     move(); 
     wraparound(pos, diameter); 
     shape(asteroid, pos.x, pos.y);
  }
  
  /**
   * Getter for asteroid's current position
   * 
   * @return The asteroid's current position
   */
  public PVector pos() {
    return pos;  
  }
  
  /**
   * Getter for asteroid's diameter
   * 
   * @return The asteroid's diameter
   */
  public int diameter() {
    return diameter;  
  }
  
}
